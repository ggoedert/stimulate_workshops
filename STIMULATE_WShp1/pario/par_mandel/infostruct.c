#include <mpi.h>

#include "infostruct.h"

void create_infostruct_datatype(MPI_Datatype *infostruct_mpi_datatype)
{
  int blocklength[] = {1, 1, 1, 1, 1, 1, 1, 1, 1};
  MPI_Datatype types[] = {MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_INT};
  MPI_Datatype tmp_datatype;
  MPI_Aint displs[9], base, extent;

  _infostruct dummy_info[2];

  MPI_Get_address(dummy_info, &base);
  MPI_Get_address(&dummy_info[0].type, displs);
  displs[0] -= base;
  MPI_Get_address(&dummy_info[0].width, displs + 1);
  displs[1] -= base;
  MPI_Get_address(&dummy_info[0].height, displs + 2);
  displs[2] -= base;
  MPI_Get_address(&dummy_info[0].numprocs, displs + 3);
  displs[3] -= base;
  MPI_Get_address(&dummy_info[0].xmin, displs + 4);
  displs[4] -= base;
  MPI_Get_address(&dummy_info[0].xmax, displs + 5);
  displs[5] -= base;
  MPI_Get_address(&dummy_info[0].ymin, displs + 6);
  displs[6] -= base;
  MPI_Get_address(&dummy_info[0].ymax, displs + 7);
  displs[7] -= base;
  MPI_Get_address(&dummy_info[0].maxiter, displs + 8);
  displs[8] -= base;
  MPI_Get_address(dummy_info + 1, &extent);
  extent -= base;

  MPI_Type_create_struct(9, blocklength, displs, types, &tmp_datatype);
  MPI_Type_create_resized(tmp_datatype, 0, extent, infostruct_mpi_datatype);
  MPI_Type_free(&tmp_datatype);
  MPI_Type_commit(infostruct_mpi_datatype);
}
