#include <mpi.h>
#include <stdlib.h>

#include <pnetcdf.h>

#include "mandelpnetcdf.h"
#include "infostruct.h"

#ifdef MULTI_FILE_JUQUEEN
  #include <mpix.h>
#endif

/* open pnetcdf file */
void open_pnetcdf(int *fh, int *var_id, const _infostruct *infostruct, const int *blocksize, const int *start, int rank)
{
  int dim_ids[2];
  double attr_data[4];
  MPI_Info info;

#ifdef MULTI_FILE_JUQUEEN
  MPI_Comm lComm;
  char number_str[5];
  int numprocs;

  MPIX_Pset_same_comm_create(&lComm);
  MPI_Comm_size(lComm, &numprocs);
  sprintf(number_str,"%d",infostruct->numprocs/numprocs);
  MPI_Info_create(&info);
  MPI_Info_set(info, "nc_num_subfiles", number_str);
#else
  info = MPI_INFO_NULL;
#endif

  ncmpi_create(MPI_COMM_WORLD, "simple.nc", NC_CLOBBER | NC_64BIT_OFFSET, info, fh);

  ncmpi_def_dim(*fh,"height",infostruct->height,&dim_ids[0]);
  ncmpi_def_dim(*fh,"width",infostruct->width,&dim_ids[1]);

  ncmpi_put_att_int(*fh, NC_GLOBAL, "type", NC_INT, 1, &infostruct->type);
  ncmpi_put_att_int(*fh, NC_GLOBAL, "numprocs", NC_INT, 1, &infostruct->numprocs);
  ncmpi_put_att_int(*fh, NC_GLOBAL, "maxiter", NC_INT, 1, &infostruct->maxiter);

  attr_data[0] = infostruct->xmin;
  attr_data[1] = infostruct->xmax;
  attr_data[2] = infostruct->ymin;
  attr_data[3] = infostruct->ymax;

  ncmpi_put_att_double(*fh, NC_GLOBAL, "position", NC_DOUBLE, 4, attr_data);

  ncmpi_def_var(*fh,"data",NC_INT,2,dim_ids,var_id);

  ncmpi_enddef(*fh);

  if (infostruct->type != 0 && infostruct->type != 1) {
    ncmpi_begin_indep_data(*fh);
  }
}

/* write block to pnetcdf file */
void write_to_pnetcdf_file(int *fh, int *var_id, const _infostruct *infostruct, const int *iterations, int width, int height,
                           int xpos, int ypos)
{
  MPI_Offset start[2];
  MPI_Offset count[2];
  start[0] = ypos;
  start[1] = xpos;
  count[0] = height;
  count[1] = width;
  if (infostruct->type == 0 || infostruct->type == 1) {
    ncmpi_put_vara_int_all(*fh,*var_id,start,count,iterations);
  } else {
    ncmpi_put_vara_int(*fh,*var_id,start,count,iterations);
  }
}

/* read pnetcdf file */
void collect_pnetcdf(int **iterations, _infostruct *infostruct)
{
  int fh;
  int var_id;
  int dim_ids[2];
  MPI_Offset dim;
  MPI_Offset start[2];
  MPI_Offset count[2];
  double attr_data[4];

  ncmpi_open(MPI_COMM_SELF, "simple.nc", NC_NOWRITE, MPI_INFO_NULL, &fh);

  ncmpi_inq_varid(fh,"data",&var_id);
  ncmpi_inq_vardimid(fh,var_id,dim_ids);
  ncmpi_inq_dimlen(fh,dim_ids[0],&dim);
  infostruct->height = dim;
  ncmpi_inq_dimlen(fh,dim_ids[1],&dim);
  infostruct->width = dim;

  ncmpi_get_att_int(fh,NC_GLOBAL,"type",&infostruct->type);
  ncmpi_get_att_int(fh,NC_GLOBAL,"numprocs",&infostruct->numprocs);
  ncmpi_get_att_int(fh,NC_GLOBAL,"maxiter",&infostruct->maxiter);
  ncmpi_get_att_double(fh,NC_GLOBAL,"position",attr_data);

  infostruct->xmin =  attr_data[0];
  infostruct->xmax =  attr_data[1];
  infostruct->ymin =  attr_data[2];
  infostruct->ymax =  attr_data[3];

  start[0] = 0;
  start[1] = 0;
  count[0] = infostruct->height;
  count[1] = infostruct->width;

  *iterations = malloc(sizeof(int) * infostruct->width * infostruct->height);
  ncmpi_get_vara_int_all(fh,var_id,start,count,*iterations);

  ncmpi_close(fh);
}

/* close pnetcdf file */
void close_pnetcdf(int *fh, const _infostruct *infostruct, int rank)
{
  ncmpi_close(*fh);
}
