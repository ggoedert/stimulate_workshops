#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>

#include <netcdf.h>
#include <netcdf_par.h>

#include "mandelnetcdf4.h"
#include "infostruct.h"

/* open netcdf4 file */
void open_netcdf4(int *fh, int *var_id, const _infostruct *infostruct, const int *blocksize, const int *start, int rank)
{
  int dim_ids[2];
  double attr_data[4];
  MPI_Info info;
  info = MPI_INFO_NULL;

  nc_create_par("simple.nc", NC_CLOBBER | NC_NETCDF4 | NC_MPIIO, MPI_COMM_WORLD, info, fh);

  nc_def_dim(*fh,"height",infostruct->height,&dim_ids[0]);
  nc_def_dim(*fh,"width",infostruct->width,&dim_ids[1]);

  nc_put_att_int(*fh, NC_GLOBAL, "type", NC_INT, 1, &infostruct->type);
  nc_put_att_int(*fh, NC_GLOBAL, "numprocs", NC_INT, 1, &infostruct->numprocs);
  nc_put_att_int(*fh, NC_GLOBAL, "maxiter", NC_INT, 1, &infostruct->maxiter);

  attr_data[0] = infostruct->xmin;
  attr_data[1] = infostruct->xmax;
  attr_data[2] = infostruct->ymin;
  attr_data[3] = infostruct->ymax;

  nc_put_att_double(*fh, NC_GLOBAL, "position", NC_DOUBLE, 4, attr_data);

  nc_def_var(*fh,"data",NC_INT,2,dim_ids,var_id);

  nc_enddef(*fh);

  if (infostruct->type == 0 || infostruct->type == 1) {
    nc_var_par_access(*fh, *var_id, NC_COLLECTIVE);
  }
}

/* write block to netcdf4 file */
void write_to_netcdf4_file(int *fh, int *var_id, const _infostruct *infostruct, const int *iterations, int width, int height,
                           int xpos, int ypos)
{
  size_t start[2];
  size_t count[2];
  start[0] = ypos;
  start[1] = xpos;
  count[0] = height;
  count[1] = width;
  nc_put_vara_int(*fh,*var_id,start,count,iterations);
}

/* read netcdf4 file */
void collect_netcdf4(int **iterations, _infostruct *infostruct)
{
  int fh;
  int var_id;
  int dim_ids[2];
  size_t dim;
  size_t start[2];
  size_t count[2];
  double attr_data[4];

  nc_open("simple.nc", NC_NOWRITE, &fh);

  nc_inq_varid(fh,"data",&var_id);
  nc_inq_vardimid(fh,var_id,dim_ids);
  nc_inq_dimlen(fh,dim_ids[0],&dim);
  infostruct->height = dim;
  nc_inq_dimlen(fh,dim_ids[1],&dim);
  infostruct->width = dim;

  nc_get_att_int(fh,NC_GLOBAL,"type",&infostruct->type);
  nc_get_att_int(fh,NC_GLOBAL,"numprocs",&infostruct->numprocs);
  nc_get_att_int(fh,NC_GLOBAL,"maxiter",&infostruct->maxiter);
  nc_get_att_double(fh,NC_GLOBAL,"position",attr_data);

  infostruct->xmin =  attr_data[0];
  infostruct->xmax =  attr_data[1];
  infostruct->ymin =  attr_data[2];
  infostruct->ymax =  attr_data[3];

  start[0] = 0;
  start[1] = 0;
  count[0] = infostruct->height;
  count[1] = infostruct->width;

  *iterations = malloc(sizeof(int) * infostruct->width * infostruct->height);
  nc_get_vara_int(fh,var_id,start,count,*iterations);

  nc_close(fh);
}

/* close pnetcdf file */
void close_netcdf4(int *fh, const _infostruct *infostruct, int rank)
{
  nc_close(*fh);
}
