#include <stdlib.h>
#include <mpi.h>

#include "mandelmpiio.h"
#include "infostruct.h"

/* write block to mpiio file */
void write_to_mpiio_file(MPI_File *fh, MPI_Datatype *filetype, const _infostruct *infostruct, int *iterations, int width, int height, int xpos,
                         int ypos)
{
  int i;

  if (infostruct->type == 0 || infostruct->type == 1) {
      MPI_File_write_all(*fh, iterations, width * height, MPI_INT, MPI_STATUS_IGNORE);
  } else if (infostruct->type == 3) {
    MPI_File_write_at(*fh, ypos * infostruct->width + xpos, iterations, width * height, MPI_INT,
                      MPI_STATUS_IGNORE);
  } else {
    for (i = 0; i < height; ++i) {
      MPI_File_write_at(*fh, (ypos + i) * infostruct->width + xpos, iterations + i * width, width,
                        MPI_INT, MPI_STATUS_IGNORE);
    }
  }
}

/* open MPIIO file */
void open_mpiio(MPI_File *fh, MPI_Datatype *filetype, _infostruct *infostruct, int *blocksize,
                int *start, int rank)
{
  MPI_Datatype infostruct_mpi_datatype, block_type, unpadded_type;
  MPI_Offset disp, offset;
  MPI_Aint lb, extent;
  int sizes[] = {infostruct->height,infostruct->width};
  int blengths[] = {1};
  int displs[] = {rank};
  int size_infotype;

  MPI_File_open(MPI_COMM_WORLD, "simple.mpi", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, fh);
  MPI_File_set_errhandler(*fh, MPI_ERRORS_ARE_FATAL);

  create_infostruct_datatype(&infostruct_mpi_datatype);
  if (rank == 0) {
    MPI_File_write(*fh, infostruct, 1, infostruct_mpi_datatype, MPI_STATUS_IGNORE);
  }

  MPI_Type_size(infostruct_mpi_datatype, &size_infotype);
  disp = (MPI_Offset) size_infotype;
  MPI_Type_free(&infostruct_mpi_datatype);

  if (infostruct->type == 0) {
    MPI_Type_contiguous(blocksize[0] * blocksize[1], MPI_INT, &block_type);
    MPI_Type_get_extent(block_type, &lb, &extent);
    MPI_Type_indexed(1, blengths, displs, block_type, &unpadded_type);
    MPI_Type_create_resized(unpadded_type, 0, infostruct->numprocs * extent, filetype);
    MPI_Type_commit(filetype);
    MPI_Type_free(&block_type);
    MPI_Type_free(&unpadded_type);
  } else if (infostruct->type == 1) {
    MPI_Type_create_subarray(2, sizes, blocksize, start, MPI_ORDER_C, MPI_INT, filetype);
    MPI_Type_commit(filetype);
  } else {
    *filetype = MPI_INT;
  }

  MPI_File_set_view(*fh, disp, MPI_INT, *filetype, "native", MPI_INFO_NULL);
}

/* close MPIIO file */
void close_mpiio(MPI_File *fh, MPI_Datatype *filetype, const _infostruct *infostruct, int rank)
{
  if (infostruct->type == 0 || infostruct->type == 1) {
    MPI_Type_free(filetype);
  }

  MPI_File_close(fh);
}

/* read MPIIO file */
void collect_mpiio(int **iterations, _infostruct *infostruct) {
  MPI_File fh;
  MPI_File_open(MPI_COMM_SELF,"simple.mpi",MPI_MODE_RDONLY,MPI_INFO_NULL,&fh);
  MPI_File_set_errhandler(fh, MPI_ERRORS_ARE_FATAL);

  MPI_Datatype infostruct_mpi_datatype;

  create_infostruct_datatype(&infostruct_mpi_datatype);
  MPI_File_read(fh,infostruct,1,infostruct_mpi_datatype,MPI_STATUS_IGNORE);
  MPI_Type_free(&infostruct_mpi_datatype);

  *iterations = malloc(sizeof(int) * infostruct->width * infostruct->height);
  MPI_File_read(fh,*iterations,infostruct->width*infostruct->height,MPI_INT,MPI_STATUS_IGNORE);

  MPI_File_close(&fh);
}
