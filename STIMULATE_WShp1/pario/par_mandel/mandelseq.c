#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "mandelseq.h"
#include "ppmwrite.h"
#include "infostruct.h"
#include "mandelmpiio.h"
#include "mandelsion.h"
#include "mandelhdf5.h"
#include "mandelpnetcdf.h"
#include "mandelnetcdf4.h"

/* Describe usage */
static void usage(char* name)
{
  fprintf(stderr, "Usage: %s options\n\nwith the following optional options (default values in parathesis):\n\n", name);

  fprintf(stderr, "  [-f <format>] 0=sion, 1=hdf5, 2=mpiio, 3=pnetcdf, 4=netcdf4\n\n");
  exit(1);
}


int main(int argc, char* argv[])
{
  int         *iterations = NULL;
  int         *proc_distribution = NULL;
  _infostruct info_glob;

  MPI_Init(&argc, &argv);

  /* parse command line */
  int format = 0;
  int i = 1;
  while (i < argc) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
        case 'f': format = atoi(argv[++i]); break;
        default:
          fprintf(stderr, "%s\n", argv[i]);
          usage(argv[0]);
      }
    } else {
      fprintf(stderr, "%s\n", argv[i]);
      usage(argv[0]);
    }
    i++;
  }

  /* init ppm */
  ppminitsmooth(1);

  /* print info header */
  switch(format) {
    case 0: printf("using SIONlib\n"); break;
    case 1: printf("using HDF5\n"); break;
    case 2: printf("using MPIIO\n"); break;
    case 3: printf("using PNETCDF\n"); break;
    case 4: printf("using NETCDF4\n"); break;
  }

  /* read SION file */
  switch (format) {
    case 0: collect_sion(&iterations, &proc_distribution, &info_glob); break;
    case 1: collect_hdf5(&iterations, &info_glob); break;
    case 2: collect_mpiio(&iterations, &info_glob); break;
    case 3: collect_pnetcdf(&iterations, &info_glob); break;
    case 4: collect_netcdf4(&iterations, &info_glob); break;
  }

  /* print information */
  print_infostruct(&info_glob);

  /* create ppm files */
  ppmwrite(iterations, info_glob.width, info_glob.height, 0,
           info_glob.maxiter, "mandelcol.ppm");

  if (format == 0 && proc_distribution != NULL) {
    ppmwrite(proc_distribution, info_glob.width, info_glob.height, 0,
             info_glob.numprocs, "mandelcol_procs.ppm");
  }

  free(iterations);
  if (proc_distribution != NULL) {
    free(proc_distribution);
  }

  MPI_Finalize();

  return EXIT_SUCCESS;
}

/*
 * Print infostruct data
 */
void print_infostruct(const _infostruct* info)
{
  printf("type      = %d\n", info->type);
  printf("width     = %d\n", info->width);
  printf("height    = %d\n", info->height);
  printf("numprocs  = %d\n", info->numprocs);
  printf("xmin      = %g\n", info->xmin);
  printf("xmax      = %g\n", info->xmax);
  printf("ymin      = %g\n", info->ymin);
  printf("ymax      = %g\n", info->ymax);
  printf("maxiter   = %d\n", info->maxiter);
}
