#ifndef MANDELMPI_H
#define MANDELMPI_H

#include <mpi.h>
#include "hdf5.h"
#include "infostruct.h"

typedef union _io_id {
  struct {
    int sid;
  } sionlib;
  struct {
    hid_t file_id;
    hid_t dataset;
  } hdf5;
  struct {
    MPI_File fh;
    MPI_Datatype filetype;
  } mpiio;
  struct {
    int fh;
    int var_id;
  } pnetcdf;
  struct {
    int fh;
    int var_id;
  } netcdf4;
} _io_id;

void calc(int *iterations, int width, int height, double xmin, double xmax, double ymin, double ymax, int maxiter,
          double *calctime);

void calc_master(int *iterations, int width, int height, int numprocs, int *blocksize, int collect, double *calctime,
                 MPI_Datatype *commtype, double *commtime);

void calc_worker(int *iterations, _io_id *io_id, _infostruct *infostruct, int collect, double *iotime, double *calctime,
                 double *commtime, int format);

void open_file(int format, _io_id *io_id, _infostruct *infostruct, int *blocksize, int *start,
               int myid, double *iotime);

void write_to_file(int format, _io_id *io_id, const _infostruct *infostruct, int *iterations,
                   int width, int height, int xpos, int ypos, double *iotime);

void close_file(int format, _io_id *io_id, const _infostruct *infostruct, int myid, double *iotime);

#endif
