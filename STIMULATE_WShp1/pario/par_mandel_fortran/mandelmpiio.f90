module mandelmpiio
    use infostruct
    use mpi

    implicit none

    private

    public :: open_mpiio, close_mpiio, write_to_mpiio_file, collect_mpiio

contains
    ! open MPIIO file
    subroutine open_mpiio(fh, filetype, info, blocksize, start, rank)
        integer, intent(out) :: fh
        integer, intent(out) :: filetype
        type(t_infostruct), intent(in) :: info
        integer, dimension(2), intent(in) :: blocksize
        integer, dimension(2), intent(in) :: start
        integer, intent(in) :: rank

        integer :: infostruct_mpi_datatype, block_type, unpadded_type
        integer :: ierror
        integer :: size_infotype
        integer, dimension(2) :: sizes
        integer(kind=MPI_OFFSET_KIND) :: disp, offset
        integer(kind=MPI_ADDRESS_KIND) :: lb, extent

        call MPI_File_open(MPI_COMM_WORLD, "simple.mpi", ior(MPI_MODE_CREATE, MPI_MODE_WRONLY), MPI_INFO_NULL, fh, ierror)
        call MPI_File_set_errhandler(fh, MPI_ERRORS_ARE_FATAL, ierror)

        call create_infostruct_datatype(infostruct_mpi_datatype)
        if (rank == 0) then
            call MPI_File_write(fh, info, 1, infostruct_mpi_datatype, MPI_STATUS_IGNORE, ierror)
        end if
        call MPI_Type_size(infostruct_mpi_datatype, size_infotype, ierror)
        disp = size_infotype
        call MPI_Type_free(infostruct_mpi_datatype, ierror)


        if (info%type == 0) then
            call MPI_Type_contiguous(blocksize(1) * blocksize(2), MPI_INTEGER, block_type, ierror)
            call MPI_Type_get_extent(block_type, lb, extent, ierror)
            call MPI_Type_indexed(1, (/ 1 /), (/ rank /), block_type, unpadded_type, ierror)
            call MPI_Type_create_resized(unpadded_type, 0_MPI_ADDRESS_KIND, &
                int(info%numprocs, MPI_ADDRESS_KIND) * extent, filetype, ierror)
            call MPI_Type_commit(filetype, ierror)
            call MPI_Type_free(block_type, ierror)
            call MPI_Type_free(unpadded_type, ierror)
        elseif (info%type == 1) then
            sizes(1) = info%width
            sizes(2) = info%height
            call MPI_Type_create_subarray(2, sizes, blocksize, start, MPI_ORDER_FORTRAN, MPI_INTEGER, filetype, ierror)
            call MPI_Type_commit(filetype, ierror)
        else
            filetype = MPI_INTEGER
        end if

        call MPI_File_set_view(fh, disp, MPI_INTEGER, filetype, "native", MPI_INFO_NULL, ierror)
    end subroutine open_mpiio

    ! write block to MPIIO file
    subroutine write_to_mpiio_file(fh, filetype, info, iterations, width, height, xpos, ypos)
        integer, intent(in) :: fh
        integer, intent(in) :: filetype
        type(t_infostruct), intent(in) :: info
        integer, dimension(:), intent(in) :: iterations
        integer, intent(in) :: width
        integer, intent(in) :: height
        integer, intent(in) :: xpos
        integer, intent(in) :: ypos

        integer :: i
        integer :: ierror

        if (info%type == 0 .or. info%type == 1) then
            call MPI_File_write_all(fh, iterations, width * height, MPI_INTEGER, MPI_STATUS_IGNORE, ierror)
        else if (info%type == 3) then
            call MPI_File_write_at(fh, int(ypos * info%width + xpos,MPI_OFFSET_KIND), iterations, width * height, MPI_INTEGER, &
               & MPI_STATUS_IGNORE, ierror)
        else
            do i = 0, width - 1
                call MPI_File_write_at(fh, int((ypos + i) * info%width + xpos,MPI_OFFSET_KIND), iterations(i * width + 1), &
                   & width, MPI_INTEGER, MPI_STATUS_IGNORE, ierror)
            end do
        end if
    end subroutine write_to_mpiio_file

    ! close MPIIO file
    subroutine close_mpiio(fh, filetype, info, rank)
        integer, intent(inout) :: fh
        integer, intent(inout) :: filetype
        type(t_infostruct), intent(in) :: info
        integer, intent(in) :: rank

        integer :: ierror

        if (info%type == 0 .or. info%type == 1) then
            call MPI_Type_free(filetype, ierror)
        end if

        call MPI_File_close(fh, ierror)
    end subroutine close_mpiio

    ! read MPIIO file
    subroutine collect_mpiio(iterations, info)
        integer, dimension(:), pointer :: iterations
        type(t_infostruct), intent(inout) :: info
        integer :: fh
        integer :: ierror
        integer :: infostruct_mpi_datatype

        call MPI_File_open(MPI_COMM_SELF, "simple.mpi", MPI_MODE_RDONLY, MPI_INFO_NULL, fh, ierror)
        call MPI_File_set_errhandler(fh, MPI_ERRORS_ARE_FATAL, ierror)

        call create_infostruct_datatype(infostruct_mpi_datatype)
        call MPI_File_read(fh, info, 1, infostruct_mpi_datatype, MPI_STATUS_IGNORE, ierror)
        call MPI_Type_free(infostruct_mpi_datatype, ierror)

        allocate(iterations(info%width*info%height))
        call MPI_File_read(fh, iterations, info%width*info%height, MPI_INTEGER, MPI_STATUS_IGNORE, ierror)

        call MPI_File_close(fh, ierror)
    end subroutine collect_mpiio

end module mandelmpiio
