#include <stdlib.h>
#include <hdf5.h>
#include <mpi.h>

#define ROWS 5
#define COLUMNS 20

int main(int argc, char** argv)
{
    hid_t file_id; /* file handle for hdf5 file */
    hid_t group_id; /* group handle */
    hid_t dataspace_id; /* dataspace handle */
    hid_t dataset_id; /* dataset handle */
    hid_t mem_dataspace_id; /* memory dataspace */
    hsize_t dims[2]; /* data dimensions */
    hid_t attr_dataspace_id; /* attribtue dataspace */
    hid_t attribute_id; /* attribute handle */
    hid_t string_type; /* string datatype */
    hid_t plist_file_id; /* file access property */
    hid_t plist_write_id; /* write property list */
    char string[] = "data"; /* attribute content */
    int data[ROWS][COLUMNS]; /* data array */
    hsize_t start[2], count[2]; /* hyperslap information */
    int i, j;
    int numprocs, rank;

    /* init MPI */
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    /* create a new file */
    plist_file_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_file_id, MPI_COMM_WORLD, MPI_INFO_NULL);
    file_id = H5Fcreate("parallel.h5", H5F_ACC_TRUNC, H5P_DEFAULT, plist_file_id);

    /* create group */
    group_id = H5Gcreate(file_id, "data", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    /* create dataspaces */
    dims[0] = ROWS * numprocs;
    dims[1] = COLUMNS;
    dataspace_id = H5Screate_simple(2, dims, NULL);
    dims[0] = ROWS;
    mem_dataspace_id = H5Screate_simple(2, dims, NULL);
    attr_dataspace_id = H5Screate(H5S_SCALAR);

    /* create attribtue string type */
    string_type = H5Tcopy(H5T_C_S1);
    H5Tset_size(string_type, sizeof(string));
    H5Tset_strpad(string_type,H5T_STR_NULLTERM);

    /* create dataset */
    dataset_id = H5Dcreate(group_id, "dset", H5T_STD_I32LE, dataspace_id, H5P_DEFAULT,
                           H5P_DEFAULT, H5P_DEFAULT);

    /* create attribute */
    attribute_id = H5Acreate(dataset_id, "name", string_type, attr_dataspace_id, H5P_DEFAULT,
                             H5P_DEFAULT);

    /* write to attribute */
    H5Awrite(attribute_id, string_type, string);

    /* close attribute */
    H5Aclose(attribute_id);

    /* create data */
    for (i=0; i<ROWS; ++i) {
        for (j=0; j<COLUMNS; ++j) {
            data[i][j] = rank;
        }
    }

    /* Specify hyperslab in the file */
    start[0] = ROWS * rank;
    start[1] = 0;
    count[0] = ROWS;
    count[1] = COLUMNS;
    H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, start, NULL, count, NULL);

    /* Create property list for collective write */
    plist_write_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(plist_write_id, H5FD_MPIO_COLLECTIVE);

    /* write data to file */
    H5Dwrite(dataset_id, H5T_NATIVE_INT, mem_dataspace_id, dataspace_id, plist_write_id, data);

    /* close plists */
    H5Pclose(plist_write_id);
    H5Pclose(plist_file_id);

    /* close dataset */
    H5Dclose(dataset_id);

    /* close type */
    H5Tclose(string_type);

    /* close dataspaces */
    H5Sclose(attr_dataspace_id);
    H5Sclose(mem_dataspace_id);
    H5Sclose(dataspace_id);

    /* close group */
    H5Gclose(group_id);

    /* close file */
    H5Fclose(file_id);

    MPI_Finalize();

    return EXIT_SUCCESS;
}

