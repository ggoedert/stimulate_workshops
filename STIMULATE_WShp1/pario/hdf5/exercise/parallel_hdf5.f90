program helloworld
    use hdf5
    use mpi

    implicit none

    integer, parameter :: ROWS = 5
    integer, parameter :: COLUMNS = 20
    integer(kind=HID_T) :: file_id ! file handle for hdf5 file
    integer(kind=HID_T) :: group_id ! group handle
    integer(kind=HID_T) :: dataspace_id ! dataspace handle
    integer(kind=HID_T) :: mem_dataspace_id ! memory dataspace handle
    integer(kind=HID_T) :: dataset_id ! dataset handle
    integer(kind=HSIZE_T), dimension(2) :: dims; ! data dimensions
    integer(kind=HID_T) :: attr_dataspace_id ! attribute dataspace
    integer(kind=HSIZE_T), dimension(1) :: attr_dims = 0
    integer(kind=HID_T) :: attribute_id ! attribute handle
    integer(kind=HID_T) :: string_type ! string datatype
    integer(kind=HID_T) :: plist_file_id ! file access property
    integer(kind=HID_T) :: plist_write_id ! write property list
    character(len=4) :: string = "data" ! attribute content
    integer, dimension(ROWS,COLUMNS) :: data
    integer(kind=HSIZE_T), dimension(2) :: start, num
    integer :: status
    integer :: i,j
    integer :: numprocs, rank

    ! init MPI
    call MPI_Init(status)
    call MPI_Comm_size(MPI_COMM_WORLD, numprocs, status)
    call MPI_Comm_rank(MPI_COMM_WORLD, rank, status)

    ! initialize fortran interface
    call H5open_f(status)

    ! open file
    call H5Pcreate_f(H5P_FILE_ACCESS_F, plist_file_id, status)
    call H5Pset_fapl_mpio_f(plist_file_id, MPI_COMM_WORLD, MPI_INFO_NULL, status)
    call H5Fcreate_f("parallel.h5", H5F_ACC_TRUNC_F, file_id, status, access_prp=plist_file_id)

    ! create group
    call H5Gcreate_f(file_id, "data", group_id, status)

    ! create dataspaces
    dims(1) = ROWS * numprocs
    dims(2) = COLUMNS
    call H5Screate_simple_f(2, dims, dataspace_id, status)
    dims(1) = ROWS
    call H5Screate_simple_f(2, dims, mem_dataspace_id, status)
    call H5Screate_f(H5S_SCALAR_F, attr_dataspace_id, status)

    ! create attribute string type
    call H5Tcopy_f(H5T_C_S1, string_type, status)
    call H5Tset_size_f(string_type, int(len(string),HSIZE_T), status)
    call H5Tset_strpad_f(string_type, H5T_STR_NULLTERM_F, status)

    ! create dataset
    call H5Dcreate_f(group_id, "dset", H5T_STD_I32LE, dataspace_id, dataset_id, status)

    ! create attribute
    call H5Acreate_f(dataset_id, "name", string_type, attr_dataspace_id, attribute_id, status)

    ! write to attribute
    call H5Awrite_f(attribute_id, string_type, string, attr_dims, status)

    ! close attribute
    call H5Aclose_f(attribute_id, status)

    ! create data
    data = rank

    ! Create property list for collective write
    call H5Pcreate_f(H5P_DATASET_XFER_F, plist_write_id, status)
    call H5Pset_dxpl_mpio_f(plist_write_id, H5FD_MPIO_COLLECTIVE_F, status)

    ! Specify hyperslab in the file
    start(1) = ROWS * rank
    start(2) = 0
    num(1) = ROWS
    num(2) = COLUMNS
    call H5Sselect_hyperslab_f(dataspace_id, H5S_SELECT_SET_F, start, num, status)

    ! write data to file
    call H5Dwrite_f(dataset_id, H5T_NATIVE_INTEGER, data, dims, status, mem_dataspace_id, &
                  & dataspace_id, plist_write_id)

    ! close plists
    call H5Pclose_f(plist_write_id, status)
    call H5Pclose_f(plist_file_id, status)

    ! close dataset
    call H5Dclose_f(dataset_id, status)

    ! close type
    call H5Tclose_f(string_type, status)

    ! close dataspaces
    call H5Sclose_f(attr_dataspace_id, status)
    call H5Sclose_f(mem_dataspace_id, status)
    call H5Sclose_f(dataspace_id, status)

    ! close group
    call H5Gclose_f(group_id, status)

    ! close file
    call H5Fclose_f(file_id, status)

    ! shut down fortran interface
    call H5close_f(status)

    call MPI_Finalize(status)
end

