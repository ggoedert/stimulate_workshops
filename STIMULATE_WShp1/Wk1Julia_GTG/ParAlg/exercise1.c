#include <stdio.h>
#include <stdlib.h>
#include <omp.h>


int main(int argc, char *argv) {
	
	MPI_Init(&argc , &argv);
	int nprocs; MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
	int rank; MPI_Comm_rank(MPI_COMM_WORLD,&rank);

	int N = 1;
	int i,j;
	float A [1][1];
        float x [1];
	float y [1];

	for (i = 0; i < N ; i++){
		x[i] = i;
		for (j = 0; j < N; j++){
			A[i][j] = i + j;
		}
	}


	for (i = 0 ; i<N ; i++){
		y[i] = 0;
		for (j = 0; j<N ; j++){

			y[i] += A[i][j]*x[j];


		}
		printf("Rank %d : y = %f",rank,y[i]);
		printf("y[%d] = %g \n",y[i]);
	}



	return EXIT_SUCCESS;
}
