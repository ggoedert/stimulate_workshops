#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define XI ((double)rand()/RAND_MAX)

double pi = acos(-1.);

double prob(double r, double gamma);
double delta_q(double xi[], int i, int j, double qi, double qj);
double populate_sphere(int N, double x[], double y[], double z[]);

int main(int argc, char *argv) {
		
	// Populate particles
	int N = 10;
	double gamma = 0.05;
	double xi[N];
	int i;
	for (i=0 ; i<N ; i++){
		xi[i] = gamma*(XI - 0.5);
		//printf("xi[%d] = %g \n",i,xi[i]);
	}

	
	double x[N], y[N], z[N];
	double r[N];
	populate_sphere(N, x, y, z);

}


double prob(double r, double gamma){
	
	double p = gamma*(1 - r*r);
	return p;
}

double delta_q(double xi[], int i, int j, double qi, double qj){

	double dq = (xi[i] - xi[j])*qi*qj;
	return dq;
}

double populate_sphere(int N, double x[], double y[], double z[]){

	int i;
	double r, u, theta;

	fp = fopen("./output/particles_alloc.txt", "w+");
	FILE *fp;

	for (i = 0 ; i<N ; i++) {
		
		r = cbrt(XI);
		u = 2*XI -1;
		theta = 2*pi*XI;

		x[i] = r*sqrt(1 - u*u)*cos(theta);
		y[i] = r*sqrt(1 - u*u)*sin(theta);
		z[i] = r*u;
		
		fprintf(fp, "%f   %f   %f",x[i],y[i],z[i]);
	}
	
	fclose(fp);
}
