#include <mpi.h>


MPI_Comm MPI_COMM_CART;
int nprocs,myrank;
int period[3],coords[3];
int Npx,Npy,Npz,Nx,Ny,Nz;
const int Q = 19;
int nx   ;                //lattice size x
int ny  ;                //lattice size y
int nz   ;              //lattice size z (only for 3D code)

void Init_mpi(int *argc,char ***argv){

period[0] = 1;period[1] = 1;period[2] = 1;
coords[0] = 1;coords[1] = 1;coords[2] = 2;

  MPI_Init(argc,argv);

  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
  
const int ndim[] ={1,1,nprocs}; 
  MPI_Dims_create(nprocs, 3, ndim);

  MPI_Cart_create(MPI_COMM_WORLD,3,ndim,period,0,&MPI_COMM_CART);
  MPI_Comm_rank(MPI_COMM_CART,&myrank);


  Npx = ndim[0];Npy=ndim[1];Npz=ndim[2];
  Nx = nx/Npx;Ny = ny/Npy;Nz=nz/Npz;
  if ((Nx * Npx != nx)||(Ny * Npy != ny)||(Nz * Npz != nz)){
     if (myrank==0){
      }
      exit(8);
  }



 
}


void communication(double *restrict const f0,double *recvbuftopz,double *recvbufbotz){

int prevprocx,prevprocy,prevprocz;
int nextprocx,nextprocy,nextprocz;
MPI_Status status;
double sendbuftopz[(Nx)*(Ny)*Q];
double sendbufbotz[(Nx)*(Ny)*Q];




int zup[5] = {9,10,11,15,16};int zdn[5] = {12,13,14,17,18};
int xup[5] = {1,2,8,15,18}; int xdn[5] = {4,5,6,16,17};
int yup[5] = {2,3,4,9,14}; int ydn[5] = {6,7,8,11,12};

MPI_Cart_shift(MPI_COMM_CART, 0,1,&prevprocx,&nextprocx);
//MPI_Cart_shift(MPI_COMM_CART, 0,-1,myrank,prevprocx);
MPI_Cart_shift(MPI_COMM_CART, 1,1,&prevprocy,&nextprocy);
//MPI_Cart_shift(MPI_COMM_CART, 1,-1,myrank,prevprocy);
MPI_Cart_shift(MPI_COMM_CART, 2,1,&prevprocz,&nextprocz);
//MPI_Cart_shift(MPI_COMM_CART, 2,-1,myrank,prevprocz);
// printf("In communication, myrank = %d, prev = %d and next = %d",myrank,prevprocz,nextprocz);
int iz = Nz-1;
int indx = 0;
for (int ix = 0;ix<Nx;ix++){
 for (int iy  = 0;iy<Ny;iy++){
  for (int k =0;k<5;k++){
    indx = (iy + (Ny)*ix)*Q + zup[k];
    sendbuftopz[indx] = f0[((ix*Ny + iy)*Nz + iz)*Q + zup[k]];

  }
 }
}

iz = 0;
indx = 0;
for (int ix = 0;ix<Nx;ix++){
 for (int iy  = 0;iy<Ny;iy++){
  for (int k =0;k<5;k++){
    indx = (iy + (Ny)*ix)*Q + zdn[k];
    sendbufbotz[indx] = f0[((ix*Ny + iy)*Nz + iz)*Q + zdn[k]];
  }
 }
}

 MPI_Sendrecv(sendbuftopz,(Nx)*(Ny)*Q,MPI_DOUBLE,nextprocz,0,recvbufbotz,(Nx)*(Ny)*Q,MPI_DOUBLE,prevprocz,0,MPI_COMM_CART,&status);
 MPI_Sendrecv(sendbufbotz,(Nx)*(Ny)*Q,MPI_DOUBLE,prevprocz,0,recvbuftopz,(Nx)*(Ny)*Q,MPI_DOUBLE,nextprocz,0,MPI_COMM_CART,&status);


}



void myFinalize()
{
  MPI_Finalize(); 
}
