// openLBMflow v1.0.1 Copyright (C) 2013 LBMflow.
// Open Source Lattice Boltzmann Solver
// www.lbmflow.com
// open@lbmflow.com

// LICENSE
// The openLBMflow code is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
// DISCLAIMER OF WARRANTY
// The code is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// This software may contain errors that could cause failures or loss of data,
// and may be incomplete or contain inaccuracies.  You expressly acknowledge and agree
// that use of the openLBMflow software is at your sole risk.
// The openLBMflow software is provided 'AS IS' and without warranty of any kind.
//2D
// To comile use: gcc -O2 -o openLBMflow openLBMflow.c -lm

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/stat.h>
#include <string.h>
#include <dirent.h>
#include <assert.h>

// initial paramaters
int nx   = 64;                //lattice size x
int ny   = 64;                //lattice size y
int nz   = 64;               //lattice size z (only for 3D code)

double tau = 1.0;           //relaxation time
double rhoh = 2.6429;       //high density fluid (rhoh=1 for singlephase)
double rhol = 0.0734;       //low  density fluid (rhoh=1 for singlephase)
double ifaceW = 3.0;        //interface width
double G = -6.0;            //interparticular interaction potential (G=0 for singlephase)
double body_force = 2.0e-5; //gravity force
double body_force_dir = 0;  //gravity direction (0=down 90=right 180=top 270=left)
int time_total = 1000;      //total time step
int time_save = 50;         //save result to VTK image file (*.vti can be open in Paraview)
int save_rho = 1;           //save density  in output file (0=don't save, 1=save)
int save_pre = 1;           //save pressure in output file (0=don't save, 1=save)
int save_vel = 1;           //save velocity in output file (0=don't save, 1=save)
int boundary_bot = 0;       //0=periodic, 1=HBB, set half way bounce back on bottom wall
int boundary_top = 0;       //0=periodic, 1=HBB, set half way bounce back on top wall
int boundary_lef = 0;       //0=periodic, 1=HBB, set half way bounce back on left wall
int boundary_rig = 0;       //0=periodic, 1=HBB, set half way bounce back on right wall
int boundary_fro = 0;       //0=periodic, 1=HBB, set half way bounce back on front wall (only for 3D)
int boundary_bac = 0;       //0=periodic, 1=HBB, set half way bounce back on back wall (only for 3D)
double rho_boundary = 0.2;  //define density of solid walls
double top_wall_speed = 0.5;//speed of top wall for lid driven cavity
double bot_wall_speed = 0;//speed of bottom wall
int d1r = 7;               //droplet1 radius
int d1x = 10;               //droplet1 position x (for multiphase model)
int d1y = 14;               //droplet1 position y (for multiphase model)
int d1z = 14;               //droplet1 position z (for multiphase model)
int drop1 = 1.0;            //1=droplet, -1=buble
int d2r = 7;               //droplet2 radius
int d2x = 24;               //droplet2 position x (for multiphase model)
int d2y = 12;               //droplet2 position y (for multiphase model)
int d2z = 12;               //droplet2 position z (for multiphase model)
int drop2 = 1.0;            //1=droplet, -1=buble
#include "lbm_mpiaos.h"
#define wall_clock(noarg) ((double) clock()/((double) CLOCKS_PER_SEC))
#define SuppressIO


// define lattice constants D3Q19: 1/3 1/18 1/36, D2Q9: 4/9 1/9 1/36
const int D = 3;
const double wi0 = 12/36., wi1 = 2/36., wi2 = 1/36.;
// const double wi[19] = {wi0, wi1, wi2, wi1, wi2, wi1, wi2, wi1, wi2, wi2, wi1, wi2, wi2, wi1, wi2, wi2, wi2, wi2, wi2};

// velocity directions
const double ex[19] = {0, 1, 1, 0,-1,-1,-1, 0, 1, 0, 0, 0, 0, 0, 0, 1,-1,-1, 1};
const double ey[19] = {0, 0, 1, 1, 1, 0,-1,-1,-1, 1, 0,-1,-1, 0, 1, 0, 0, 0, 0};
const double ez[19] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1,-1,-1,-1, 1, 1,-1,-1};


// Half-Way bounce back for all non-zero velocities, flip velocities with their opposites
const int opposite[19] = {0, 5, 6, 7, 8, 1, 2, 3, 4, 12, 13, 14, 9, 10, 11, 17, 18, 15, 16};

typedef size_t index_t; // defines the integer data type for direct indexing

int Nx, Ny, Nz, NxNyNz; // local lattice sizes, NxNyNz >= Nx*Ny*Nz

index_t indexyz(const int x, const int y, const int z) { 
return ((x*Ny + y)*Nz + z); 
}

inline index_t phindex(const int x, const int y, const int z) { return ((x+1)*(Ny+2) + (y+1))*(Nz+2) + (z+1); } // enlarged with a halo of thickness 1
inline index_t ipop(const index_t xyz, const int k) { return k + Q*xyz; }



void solid_cell_treatment(const index_t xyz, const double *restrict const rho, const aos *restrict const fn, aos *restrict const tmp_fn) {
  printf("Why the fuck would you call solid cell treatment??!! \n");
	for (int iq = 0; iq < Q; ++iq) {
		tmp_fn[iq].p[0] = fn[xyz].p[iq];
	} // iq

	if (0 != top_wall_speed) { // moving top wall
		tmp_fn[6].p[0] -= top_wall_speed*rho[xyz]/6.0;
		tmp_fn[8].p[0] += top_wall_speed/rho[xyz]/6.0;
	} // top wall speed
	if (0 != bot_wall_speed) { // moving bottom wall
		tmp_fn[2].p[0] += bot_wall_speed*rho[xyz]/6.0;
		tmp_fn[4].p[0] -= bot_wall_speed/rho[xyz]/6.0;
	} // bottom wall speed

} // solid_cell_treatment


// propagate-kernel (performance critical code section)

void propagate(const int x, const int y, const int z, const aos *restrict const tmp_fn, aos *restrict const fn) {
	// exploit that we can assume the local domain to be periodic
	const int rig = (x >= Nx - 1)?(0):(x + 1);
	const int lef = (x <= 0)?(Nx - 1):(x - 1);
	const int top = (y >= Ny - 1)?(0):(y + 1);
	const int bot = (y <= 0)?(Ny - 1):(y - 1);
	const int bac = (z >= Nz - 1)?(0):(z + 1);
	const int fro = (z <= 0)?(Nz - 1):(z - 1);
        printf("This propagate function should not be called \n");

} // propagate


void propagate_mpi(const aos *restrict const tmp_fn, aos *restrict const fn) {
	// exploit that we can assume the local domain to be periodic
  aos recvbuftopz1[Nx*Ny];
  aos recvbufbotz1[Nx*Ny];

 communication(tmp_fn,recvbuftopz1,recvbufbotz1);

	for (int x = 0 ; x < Nx; x++) {
		for (int y = 0; y < Ny; y++) {
			for (int z = 0; z < Nz; z++) {

	const int rig = (x >= Nx - 1)?(0):(x + 1);
	const int lef = (x <= 0)?(Nx - 1):(x - 1);
	const int top = (y >= Ny - 1)?(0):(y + 1);
	const int bot = (y <= 0)?(Ny - 1):(y - 1);

	const int bac = (z >= Nz - 1)?(0):(z + 1);
	const int fro = (z <= 0)?(Nz - 1):(z - 1);

	fn[indexyz(  x,   y, z)].p[0] = tmp_fn[indexyz(  x,   y, z)].p[ 0]; //
	fn[indexyz(rig,   y, z)].p[1] = tmp_fn[indexyz(  x,   y, z)].p[ 1]; // +x
	fn[indexyz(rig, top, z)].p[2] = tmp_fn[indexyz(  x,   y, z)].p[ 2]; // +x+y
	fn[indexyz(  x, top, z)].p[3] = tmp_fn[indexyz(  x,   y, z)].p[ 3]; //   +y
	fn[indexyz(lef, top, z)].p[4] = tmp_fn[indexyz(  x,   y, z)].p[ 4]; // -x+y
	fn[indexyz(lef,   y, z)].p[5] = tmp_fn[indexyz(  x,   y, z)].p[ 5]; // -x
	fn[indexyz(lef, bot, z)].p[6] = tmp_fn[indexyz(  x,   y, z)].p[ 6]; // -x-y
	fn[indexyz(  x, bot, z)].p[7] = tmp_fn[indexyz(  x,   y, z)].p[ 7]; //   -y
	fn[indexyz(rig, bot, z)].p[8] = tmp_fn[indexyz(  x,   y, z)].p[ 8]; // +x-y

if (bac==0){

  fn[indexyz(x, top, bac)].p[ 9] = recvbufbotz1[(x*Ny + y)].p[ 9]; //   +y+z
  fn[indexyz(x,   y, bac)].p[10] = recvbufbotz1[(x*Ny + y)].p[10]; //     +z
  fn[indexyz(x, bot, bac)].p[11] = recvbufbotz1[(x*Ny + y)].p[11]; //   -y+z
  fn[indexyz(rig, y, bac)].p[15] = recvbufbotz1[(x*Ny + y)].p[15]; // +x  +z
  fn[indexyz(lef, y, bac)].p[16] = recvbufbotz1[(x*Ny + y)].p[16]; // -x  +z
} else{
	fn[indexyz(x, top, bac)].p[ 9] = tmp_fn[indexyz(  x,   y, z)].p[ 9]; //   +y+z
	fn[indexyz(x,   y, bac)].p[10] = tmp_fn[indexyz(  x,   y, z)].p[10]; //     +z
	fn[indexyz(x, bot, bac)].p[11] = tmp_fn[indexyz(  x,   y, z)].p[11]; //   -y+z
	fn[indexyz(rig, y, bac)].p[15] = tmp_fn[indexyz(  x,   y, z)].p[15]; // +x  +z
	fn[indexyz(lef, y, bac)].p[16] = tmp_fn[indexyz(  x,   y, z)].p[16]; // -x  +z
}

if (fro==Nz-1){
  fn[indexyz(lef, y, fro)].p[17] = recvbuftopz1[(x*Ny + y)].p[17]; // -x  -z
  fn[indexyz(rig, y, fro)].p[18] = recvbuftopz1[(x*Ny + y)].p[18]; // +x  -z
  fn[indexyz(x, bot, fro)].p[12] = recvbuftopz1[(x*Ny + y)].p[12]; //   -y-z
  fn[indexyz(x,   y, fro)].p[13] = recvbuftopz1[(x*Ny + y)].p[13]; //     -z
  fn[indexyz(x, top, fro)].p[14] = recvbuftopz1[(x*Ny + y)].p[14]; //   +y-z

} else {
  fn[indexyz(lef, y, fro)].p[17] = tmp_fn[indexyz(  x,   y, z)].p[17];
  fn[indexyz(rig, y, fro)].p[18] = tmp_fn[indexyz(  x,   y, z)].p[18];
  fn[indexyz(x, bot, fro)].p[12] = tmp_fn[indexyz(  x,   y, z)].p[12];
  fn[indexyz(x,   y, fro)].p[13] = tmp_fn[indexyz(  x,   y, z)].p[13];
  fn[indexyz(x, top, fro)].p[14] = tmp_fn[indexyz(  x,   y, z)].p[14];
}
}
}
}
} // propagate

// kernel (performance critical code section)
void update(const char *restrict const solid,
		const double *restrict const body_force_xyz,
		aos *restrict  fp, //  input populations
		aos *restrict const fn, // output populations
		double *restrict const rho, // density
		double *restrict const ux, double *restrict const uy, double *restrict const uz, // macroscopic velocities
		double *restrict const phi) {

	const double inv_tau = 1.0/tau, min_tau = 1.0 - inv_tau; // relaxation time constants
					aos tmp_fn[Nx*Ny*Nz];

	// calculate rho, ux, uy, uz and phi



 		
	for (int x = 0; x < Nx; x++) {
		for (int y = 0; y < Ny; y++) {
			for (int z = 0; z < Nz; z++) {
				const index_t xyz = indexyz(x, y, z);

				if (!solid[xyz]) {
					//calculate rho and ux, uy
#define f(K) fp[xyz].p[K]
					const double tmp_rho = f(0)+f(1)+f(2)+f(3)+f(4)+f(5)+f(6)+f(7)+f(8)+f(9)+f(10)+f(11)+f(12)+f(13)+f(14)+f(15)+f(16)+f(17)+f(18);
					double tmp_ux = (f(1)+f(2)+f(8)-f(4)-f(5)-f(6)+f(15)+f(18)-f(16)-f(17))/tmp_rho;
					double tmp_uy = (f(2)+f(3)+f(4)-f(6)-f(7)-f(8)+f(9)+f(14)-f(11)-f(12))/tmp_rho;
					double tmp_uz = (f(9)+f(10)+f(11)-f(12)-f(13)-f(14)+f(15)+f(16)-f(17)-f(18))/tmp_rho;
#undef f  // abbreviation

					// add the body force
					tmp_ux += tau*body_force_xyz[0];
					tmp_uy += tau*body_force_xyz[1];
					tmp_uz += tau*body_force_xyz[2];
					rho[xyz] = tmp_rho; // store density


					ux[xyz] = tmp_ux;
					uy[xyz] = tmp_uy;
					uz[xyz] = tmp_uz;
					const double uxyz2 = tmp_ux*tmp_ux + tmp_uy*tmp_uy + tmp_uz*tmp_uz;
					const double ux2 = tmp_ux*tmp_ux;
					const double uy2 = tmp_uy*tmp_uy;
					const double uz2 = tmp_uz*tmp_uz;
					const double uxy2 = ux2 + uy2;
					const double uyz2 = uy2 + uz2;
					const double uzx2 = uz2 + ux2;
					const double uxy = 2.0*tmp_ux*tmp_uy;
					const double uyz = 2.0*tmp_uy*tmp_uz;
					const double uzx = 2.0*tmp_uz*tmp_ux;

					//                  weights: wi[19] = {wi0, wi1, wi2, wi1, wi2, wi1, wi2, wi1, wi2, wi2, wi1, wi2, wi2, wi1, wi2, wi2, wi2, wi2, wi2};

					tmp_fn[xyz].p[ 0] = fp[xyz].p[ 0]*min_tau + (wi0*tmp_rho*(1.0                                             - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[ 1] = fp[xyz].p[ 1]*min_tau + (wi1*tmp_rho*(1.0 + 3.0*(+tmp_ux         ) + 4.5*(ux2       ) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[ 2] = fp[xyz].p[ 2]*min_tau + (wi2*tmp_rho*(1.0 + 3.0*(+tmp_ux + tmp_uy) + 4.5*(uxy2 + uxy) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[ 3] = fp[xyz].p[ 3]*min_tau + (wi1*tmp_rho*(1.0 + 3.0*(+tmp_uy         ) + 4.5*(uy2       ) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[ 4] = fp[xyz].p[ 4]*min_tau + (wi2*tmp_rho*(1.0 + 3.0*(-tmp_ux + tmp_uy) + 4.5*(uxy2 - uxy) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[ 5] = fp[xyz].p[ 5]*min_tau + (wi1*tmp_rho*(1.0 - 3.0*(+tmp_ux         ) + 4.5*(ux2       ) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[ 6] = fp[xyz].p[ 6]*min_tau + (wi2*tmp_rho*(1.0 + 3.0*(-tmp_ux - tmp_uy) + 4.5*(uxy2 + uxy) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[ 7] = fp[xyz].p[ 7]*min_tau + (wi1*tmp_rho*(1.0 - 3.0*(+tmp_uy         ) + 4.5*(uy2       ) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[ 8] = fp[xyz].p[ 8]*min_tau + (wi2*tmp_rho*(1.0 + 3.0*(+tmp_ux - tmp_uy) + 4.5*(uxy2 - uxy) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[ 9] = fp[xyz].p[ 9]*min_tau + (wi2*tmp_rho*(1.0 + 3.0*(+tmp_uy + tmp_uz) + 4.5*(uyz2 + uyz) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[10] = fp[xyz].p[10]*min_tau + (wi1*tmp_rho*(1.0 + 3.0*(+tmp_uz         ) + 4.5*(uz2       ) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[11] = fp[xyz].p[11]*min_tau + (wi2*tmp_rho*(1.0 + 3.0*(-tmp_uy + tmp_uz) + 4.5*(uyz2 - uyz) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[12] = fp[xyz].p[12]*min_tau + (wi2*tmp_rho*(1.0 + 3.0*(-tmp_uy - tmp_uz) + 4.5*(uyz2 + uyz) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[13] = fp[xyz].p[13]*min_tau + (wi1*tmp_rho*(1.0 + 3.0*(-tmp_uz         ) + 4.5*(uz2       ) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[14] = fp[xyz].p[14]*min_tau + (wi2*tmp_rho*(1.0 + 3.0*(+tmp_uy - tmp_uz) + 4.5*(uyz2 - uyz) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[15] = fp[xyz].p[15]*min_tau + (wi2*tmp_rho*(1.0 + 3.0*(+tmp_ux + tmp_uz) + 4.5*(uzx2 + uzx) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[16] = fp[xyz].p[16]*min_tau + (wi2*tmp_rho*(1.0 + 3.0*(-tmp_ux + tmp_uz) + 4.5*(uzx2 - uzx) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[17] = fp[xyz].p[17]*min_tau + (wi2*tmp_rho*(1.0 + 3.0*(-tmp_ux - tmp_uz) + 4.5*(uzx2 + uzx) - 1.5*uxyz2))*inv_tau;
					tmp_fn[xyz].p[18] = fp[xyz].p[18]*min_tau + (wi2*tmp_rho*(1.0 + 3.0*(+tmp_ux - tmp_uz) + 4.5*(uzx2 - uzx) - 1.5*uxyz2))*inv_tau;
					// the loops for collide and propate are merged, otherwise we would need to store tmp_fn back into fp
				} // solid
			} // z
		} // y
	} // x


					propagate_mpi(tmp_fn, fn); // writes into fn, also propagates into solid boundary cells


	// we have to treat the solid cells since some velocities may have penetrated them



} // update


//
//
// initialization functions rho, ux, uy, uz (not critical for performance)
//
//

void initialize_boundary(const int boundary_lef, const int boundary_rig,
		const int boundary_bot, const int boundary_top,
		const int boundary_fro, const int boundary_bac,
		const double rho_boundary,
		char *restrict const solid, double *restrict const rho,
		double *restrict const ux, double *restrict const uy, double *restrict const uz) {

	// initialize type of cells
	
	for (index_t xyz = 0; xyz < Nx*Ny*Nz; xyz++) solid[xyz] = 0; // init all with liquid

	const double rho_solid = rho_boundary*(rhoh - rhol) + rhol;

	// define Bounce Back Boundary condition
	for (int x = 0; x < Nx; x++) {
		for (int z = 0; z < Nz; z++) {
			if (0 < boundary_bot) {
				const index_t xyz = indexyz(x, 0, z); // node on bottom boundary, y==min
				solid[xyz] = 1;
				rho[xyz] = rho_solid;
				if (0 != bot_wall_speed) { ux[xyz] = bot_wall_speed; uy[xyz] = 0; uz[xyz] = 0; }
			}
			if (0 < boundary_top) {
				const index_t xyz = indexyz(x, Ny - 1, z); // node on top boundary, y==max
				solid[xyz] = 2;
				rho[xyz] = rho_solid;
				if (0 != top_wall_speed) { ux[xyz] = top_wall_speed; uy[xyz] = 0; uz[xyz] = 0; }
			}
		} // z
	} // x

	for (int y = 0; y < Ny; y++) {
		for (int z = 0; z < Nz; z++) {
			if (0 < boundary_lef) {
				const index_t xyz = indexyz(0, y, z); // node on left boundary, x==min
				solid[xyz] = 3;
				rho[xyz] = rho_solid;
			}
			if (0 < boundary_rig) {
				const index_t xyz = indexyz(Nx - 1, y, z); // node on right boundary, x==max
				solid[xyz] = 4;
				rho[xyz] = rho_solid;
			}
		} // z
	} // y

	for (int x = 0; x < Nx; x++) {
		for (int y = 0; y < Ny; y++) {
			if (0 < boundary_fro) {
				const index_t xyz = indexyz(x, y, 0); // node on front boundary, z==min
				solid[xyz] = 5;
				rho[xyz] = rho_solid;
			}
			if (0 < boundary_bac) {
				const index_t xyz = indexyz(x, y, Nz - 1); // node on back boundary, z==max
				solid[xyz] = 6;
				rho[xyz] = rho_solid;
			}
		} // y
	} // x

} // initialize_boundary

void initialize_density(const double value, const char *restrict const solid, double *restrict const rho) {
	for (index_t xyz = 0; xyz < Nx*Ny*Nz; xyz++) {
		if (!solid[xyz]) rho[xyz] = value;
	} // xyz
} // initialize_density


void initialize_distrFunc(const char *restrict const solid,  const double *restrict const rho,
		double *restrict const ux, double *restrict const uy, double *restrict const uz,
		aos *restrict const f0, aos *restrict const f1, double body_force_xyz[]) {

	// set body_force vector (global variable body_force_xyz)
	const double arg = body_force_dir/(180.0/M_PI);
	body_force_xyz[0] =  body_force*sin(arg);
	body_force_xyz[1] = -body_force*cos(arg);
	body_force_xyz[2] = 0.0;

	const double uvec[3] = {0.0, 0.0, 0.0};

	const double wi[19] = {wi0, wi1, wi2, wi1, wi2, wi1, wi2, wi1, wi2, wi2, wi1, wi2, wi2, wi1, wi2, wi2, wi2, wi2, wi2};

	// check opposite index array (self-check)
	for (int iq = 0; iq < Q; ++iq) {
		assert(ex[iq] == -ex[opposite[iq]]);
		assert(ey[iq] == -ey[opposite[iq]]);
		assert(ez[iq] == -ez[opposite[iq]]);
	}

	for (index_t xyz = 0; xyz < Nx*Ny*Nz; xyz++) {
		if (!solid[xyz]) {
			ux[xyz] = uvec[0];
			uy[xyz] = uvec[1];
			uz[xyz] = uvec[2];

			const double vel3 = ux[xyz]*ux[xyz] + uy[xyz]*uy[xyz] + uz[xyz]*uz[xyz];
			for (int k = 0; k < Q; k++) {
				const double vel1 = ex[k]*ux[xyz] + ey[k]*uy[xyz] + ez[k]*uz[xyz];
				const double vel2 = vel1*vel1;
				const double feq = wi[k]*rho[xyz]*(1.0 + 3.0*vel1 + 4.5*vel2 - 1.5*vel3);
				f0[xyz].p[k] = feq;
				f1[xyz].p[k] = feq;
			} // k
		} // solid
	} // xyz
} // initialize_distrFunc



double massConservation(const double *restrict const rho) {
  double mass_l  = 0.0;
  double mass = 0;
	for (index_t xyz = 0; xyz < Nx*Ny*Nz; xyz++){
		mass_l += rho[xyz];
	}
	MPI_Reduce(&mass_l,&mass,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_CART);
	return mass;
} // massConservation

//
//
// vizualization functions (not critical for performance)
//
//
inline double square(const double v) { return v*v; }

void write_collection_pvd(const int t, const int nx, const int ny, const int nz, const char *directory, const char *filename) {
#ifdef SuppressIO
	return;
#endif
	char dataFileName[255];
	FILE *dataFile;
	int dir = mkdir(directory, 0777); // delete the second argument in WIN32
	// if (0 == dir) printf("Error: Cannot create output directory!\n");
	sprintf(dataFileName,"%s/%s.pvd",directory,filename);
	dataFile = fopen(dataFileName,"w");
	fprintf(dataFile, "<?xml version=\"1.0\"?>\n"
			"<!-- openLBMflow v1.0.1, www.lbmflow.com -->\n"
			"<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\">\n"
			"  <Collection>\n");
	for (int tt = 0; tt <= time_total; tt += time_save) {
		fprintf(dataFile, "    <DataSet  timestep=\"%d\" group=\"\" part=\"%d\" file=\"%s_%07d.vti\"/>\n",tt,0,filename,tt);
	}
	fprintf(dataFile, "  </Collection>\n"
			"</VTKFile>\n");
	fclose(dataFile);
} // write_collection_pvd

#if 0
typedef float Float; // defines the data type for visualization
#else
typedef double Float; // defines the data type for visualization
#endif

void writeVTK(const int t, const int nx, const int ny, const int nz, // time and sizes
		const Float rho[], const int write_rho, // density
		const Float pre[], const int write_pre, // pressure
		const Float ux[], const Float uy[], const Float uz[], const int write_vel, // velocities
		const char *directory, const char *filename) { // file
#ifdef SuppressIO
	return;
#endif
	char dataFileName[255];
	FILE *dataFile;
	int dir = mkdir(directory, 0777); // delete the second argument in WIN32
	if (0 == dir) printf("Error: Can't create output directory!\n");
	sprintf(dataFileName, "%s/%s_%07d.vti", directory, filename, t);
	dataFile = fopen(dataFileName, "w");
	fprintf(dataFile, "<?xml version=\"1.0\"?>\n"
			"<!-- openLBMflow v1.0.1, www.lbmflow.com -->\n"
			"<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	fprintf(dataFile, "  <ImageData WholeExtent=\"0 %d 0 %d 0 %d\" Origin=\"0 0 0\" Spacing=\"1 1 1\">\n", nx-1, ny-1, nz-1);
	fprintf(dataFile, "  <Piece Extent=\"0 %d 0 %d 0 %d\">\n", nx-1, ny-1, nz-1);
	fprintf(dataFile, "    <PointData Scalars=\"scalars\">\n");

	if (0 != write_rho) { // write density
		fprintf(dataFile, "      <DataArray type=\"Float32\" Name=\"Density\" NumberOfComponents=\"1\" format=\"ascii\">\n");
		for (int z = 0; z < nz; z++) {
			for (int y = 0; y < ny; y++) {
				for (int x = 0; x < nx; x++) {
					fprintf(dataFile,"%.4e ", rho[(x*ny + y)*nz + z]);
				} // x
				fprintf(dataFile, "\n");
			} // y
		} // z
		fprintf(dataFile, "      </DataArray>\n");
	} // write_rho

	if (0 != write_pre) { // write pressure
		fprintf(dataFile, "      <DataArray type=\"Float32\" Name=\"Pressure\" NumberOfComponents=\"1\" format=\"ascii\">\n");
		for (int z = 0; z < nz; z++) {
			for (int y = 0; y < ny; y++) {
				for (int x = 0; x < nx; x++) {
					fprintf(dataFile,"%.4e ", pre[(x*ny + y)*nz + z]);
				} // x
				fprintf(dataFile, "\n");
			} // y
		} // z
		fprintf(dataFile, "      </DataArray>\n");
	} // write_pre

	if (0 != write_vel) { // write velocity
		fprintf(dataFile, "      <DataArray type=\"Float32\" Name=\"Velocity\" NumberOfComponents=\"3\" format=\"ascii\">\n");
		for (int z = 0; z < nz; z++) {
			for (int y = 0; y < ny; y++) {
				for (int x = 0; x < nx; x++) {
					const size_t xyz = (x*ny + y)*nz + z; // global index
					fprintf(dataFile,"%.4e %.4e %.4e ", ux[xyz], uy[xyz], uz[xyz]);
				} // x
				fprintf(dataFile, "\n");
			} // y
		} // z
		fprintf(dataFile, "      </DataArray>\n");
	} // write_vel

	fprintf(dataFile, "    </PointData>\n"
			"    <CellData>\n"
			"    </CellData>\n"
			"  </Piece>\n"
			"  </ImageData>\n"
			"</VTKFile>\n");
	fclose(dataFile);
} // writeVTK


char*   init_mem_char  (const long num) { char   *p = (char*)   malloc(num*sizeof(char));   assert(p); for (int i = 0; i < num; ++i) p[i] = 0;   return p; }
double* init_mem_double(const long num) { double *p = (double*) malloc(num*sizeof(double)); assert(p); for (int i = 0; i < num; ++i) p[i] = 0.0; return p; }
Float*  init_mem_Float (const long num) { Float  *p = (Float*)  malloc(num*sizeof(Float));  assert(p); for (int i = 0; i < num; ++i) p[i] = 0.0; return p; }

double outputSave(const int t, const double rho[], const double ux[], const double uy[], const double uz[], const double phi[], const int ranks[], const int myrank) {
	static double timer_start, step_start;

	double time_stop = wall_clock(); // stop internal timer

	// calculate performance in units of Mega Lattice Site Update per second: MLUP/s
	const double Speed = (t)?((nx*ny)*(nz*1.e-6)*(t - step_start)/(time_stop - timer_start)):0; // use global lattice sizes nx,ny,nz
	step_start = t;

	double mass = massConservation(rho);

	const int is_master = (0 == myrank)?1:0;
	if (myrank==0) printf("t=%d\tSpeed=%f MLUP/s mass=%f\n", t, Speed, mass);

	const int nall = nx*ny*nz, offs[] = {ranks[0]*Nx, ranks[1]*Ny, ranks[2]*Nz};
	Float *rho_all = NULL, *pre_all = NULL, *vel_all = NULL;
	if (save_rho) rho_all = init_mem_Float(nall);
	if (save_pre) pre_all = init_mem_Float(nall);
	if (save_vel) vel_all = init_mem_Float(nall*D);

	for (int x = 0; x < Nx; x++) {
		for (int y = 0; y < Ny; y++) {
			for (int z = 0; z < Nz; z++) {
				const index_t xyz = indexyz(x, y, z); // local index into rho, ux, uy, uz
				const size_t gxyz = ((x + offs[0])*ny + (y + offs[1]))*nz + (z + offs[2]); // global index
				if (save_rho) rho_all[gxyz] = rho[xyz];
				if (save_pre) pre_all[gxyz] = rho[xyz]/3.0

						;
				if (save_vel) {
					vel_all[gxyz + 0*nall] = ux[xyz];
					vel_all[gxyz + 1*nall] = uy[xyz];
					vel_all[gxyz + 2*nall] = uz[xyz];
				} // velocities
			} // z
		} // y
	} // x

	if (is_master) writeVTK(t, nx, ny, nz, rho_all, save_rho, pre_all, save_pre, &vel_all[0*nall], &vel_all[1*nall], &vel_all[2*nall], save_vel, "output", "openLBMflow");

	if (save_rho) free(rho_all);
	if (save_pre) free(pre_all);
	if (save_vel) free(vel_all);

	timer_start = wall_clock(); // start internal timer again
	return Speed;
} // outputSave


int main(int argc, char **argv) {

	Init_mpi(&argc,&argv);
  printf("Nx %d, Ny %d, Nz %d, indexyz %d \n",Nx,Ny,Nz,indexyz(2,4,8));

	NxNyNz = Nx*Ny*Nz; // here is some potential for memory alignment, but watch out for loop limits

	if(myrank==0)printf("openLBMflow v1.0.0 (c) 2010 www.lbmflow.com\n");

	double body_force_xyz[3];

	// allocate memory
	const long t_mem = (nx*ny*nz)*(1 + 8*(2*Q + 5)); // does not account for phi-halos
	if(myrank==0)printf("LBM  needs %.3f GiByte total for D%dQ%d with (%d x %d x %d) cells.\n",
			t_mem/1073741824.,       D, Q,      nx,  ny,  nz);

	char *solid = init_mem_char(NxNyNz); // one Byte per cell
        aos *f0, *f1;
	f0  = (aos *) malloc(NxNyNz*sizeof(aos));

	f1  = (aos *) malloc(NxNyNz*sizeof(aos));
	double *rho = init_mem_double(NxNyNz);
	double *ux  = init_mem_double(NxNyNz);
	double *uy  = init_mem_double(NxNyNz);
	double *uz  = init_mem_double(NxNyNz);

	double *phi = NULL;
	rhol = 1.0; rhoh = 1.0; G = 0.0;


	initialize_boundary(boundary_lef, boundary_rig, boundary_bot, boundary_top, boundary_fro, boundary_bac,
			rho_boundary, solid, rho, ux, uy, uz);


	initialize_density(rhol, solid, rho);



	initialize_distrFunc(solid, rho, ux, uy, uz, f0, f1, body_force_xyz);


	double speed_stats[] = {0, 0, 0}, speed;

#ifdef TIME_TOTAL
#define time_total TIME_TOTAL  // control the maximum number of iterations at compile time
#endif

#ifdef TIME_SAVE
#define time_save  TIME_SAVE   // control the .vti-file output frequency at compile time
#endif

	assert(time_save%2 == 0); // if the check point interval is an odd number, we cannot combine two time steps

	// main iteration loop


	for (int t = 0; t <= time_total; t+=2) {
		if (0 == t%time_save) {
			const int offs[] = {0, 0, 0};
			speed = outputSave(t, rho, ux, uy, uz, phi, offs, myrank); // save output to VTK image file
			if (speed > 0) { speed_stats[0] += 1.; speed_stats[1] += speed; speed_stats[2] += speed*speed; }
		} // measure and dump data as .vti files


		// calculate the distribution function for the next two time steps


		update(solid, body_force_xyz, f0, f1, rho, ux, uy, uz, phi);


		update(solid, body_force_xyz, f1, f0, rho, ux, uy, uz, phi);


	} // t

	write_collection_pvd(0, nx, ny, nz, "output", "openLBMflow");

	// compute mean and deviation
	speed = ((speed_stats[0] > 0)?1.0/speed_stats[0]:1.0); // inverse denominator
	speed_stats[1] *= speed;
	speed_stats[2] *= speed; speed_stats[2] -= speed_stats[1]*speed_stats[1]; speed_stats[2] = fabs(speed_stats[2]);
	speed_stats[2] *= ((speed_stats[0] > 1)?speed_stats[0]/(speed_stats[0] - 1.0):1.0); // Gaussian variance
	double speed0,speed1;
	speed0 = speed_stats[1];
	MPI_Reduce(&speed0,&speed1,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_CART);
	speed1 = speed1/nprocs;
	if (myrank==0) printf("LBM  avgSpeed %.3f +/- %.3f MLUP/s\n", speed1, sqrt(speed_stats[2]));

	// free allocated memory
	free(solid);
	free(f0);
	free(f1);
	free(rho);
	free(ux);
	free(uy);
	free(uz);
	if (phi) free(phi);
	MPI_Finalize();
} // main
