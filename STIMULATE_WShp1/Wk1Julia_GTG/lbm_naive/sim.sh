#!/bin/bash

for nt in 1 2 4 6 8 10 14 16 20 24 28 
do 
    make clean && make NT=${nt} 
    srun ./lbm  
done
