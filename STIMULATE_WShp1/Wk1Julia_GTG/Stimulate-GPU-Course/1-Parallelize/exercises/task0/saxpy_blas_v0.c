// Copyright (c) 2017-2018, NVIDIA CORPORATION. All rights reserved.
#include <stdio.h>
#include <stdlib.h>

//TODO-DONE: include the cublas_v2 header and cuda_runtime.h
#include <cuda_runtime.h>
#include "cublas_v2.h"
//TODO-DONE: after that replace the following `#if 0` with '#if 1' to enable the error checking macro CUDA_RT_CALL
// Note: CUBLAS documentation is available at https://docs.nvidia.com/cuda/cublas/index.html

#if 1
#ifndef CUDA_RT_CALL
#define CUDA_RT_CALL( call )                                                                       \
{                                                                                                  \
    cudaError_t cudaStatus = call;                                                                 \
    if ( cudaSuccess != cudaStatus )                                                               \
        fprintf(stderr, "ERROR: CUDA RT call \"%s\" in line %d of file %s failed with %s (%d).\n", \
                        #call, __LINE__, __FILE__, cudaGetErrorString(cudaStatus), cudaStatus);    \
}
#endif //CUDA_RT_CALL
#endif


void saxpy(float a, float* x, float* y, int n)
{
  for (int i = 0; i < n; ++i)
      y[i] = a*x[i] + y[i];
}



int main(int argc, char** argv){

    
    
    const int N = 1<<20;
    float a = 2.0;

    float* x = (float*) malloc(N * sizeof(float));
    float* y = (float*) malloc(N * sizeof(float));
    
    // set x and y to some initial values
    for (int i = 0; i < N; ++i) x[i] = 1.0;
    for (int i = 0; i < N; ++i) y[i] = 10.0;
    

    printf("Initial values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }


    // TODO: initialize cublas
    // use the incomplete status line and the following if condition for error checking
    cublasStatus_t status;
    if (status != CUBLAS_STATUS_SUCCESS) {
        fprintf(stderr, "ERROR: CUBLAS initialization error\n");
    return -1;
    }
    

    // device pointers
    float* d_x;
    float* d_y;
    // TODO: Allocate device memory, CUDA_RT_CALL is a macro that checks the erroe code
    //CUDA_RT_CALL( ...);

    
    //TODO: Set the device vectors with values from the host vectors 


       
    //TODO: replace the saxpy call with the cublas version
    // status = ...;cublasSaxpy(handle, N, &a, d_x, 1, d_y, 1);
    saxpy(a, x, y, N);
    

    //TODO: Get the result vector back from the device
 


    //TODO: shutdown cublas

    // TODO: Free device pointers 
    //CUDA_RT_CALL(...);
    //CUDA_RT_CALL(cudaFree(d_y));

    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }


    free(x);
    free(y);

}
