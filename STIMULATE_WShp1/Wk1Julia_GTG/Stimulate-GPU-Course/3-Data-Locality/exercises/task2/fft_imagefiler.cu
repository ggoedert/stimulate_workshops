/* Copyright (c) 2017-2018, NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cassert>
#include <complex>

#include <fftw3.h>

#include <cufft.h>

#ifndef CUFFT_CALL
#define CUFFT_CALL( call )                                                                                                           \
{                                                                                                                                    \
    cufftResult cufftStatus = call;                                                                                                  \
    if ( CUFFT_SUCCESS != cufftStatus )                                                                                              \
        fprintf(stderr, "ERROR: CUFFT call \"%s\" in line %d of file %s failed with %d.\n", #call, __LINE__, __FILE__, cufftStatus); \
}
#endif //CUFFT_CALL

#ifndef CUDA_RT_CALL
#define CUDA_RT_CALL( call )                                                                       \
{                                                                                                  \
    cudaError_t cudaStatus = call;                                                                 \
    if ( cudaSuccess != cudaStatus )                                                               \
        fprintf(stderr, "ERROR: CUDA RT call \"%s\" in line %d of file %s failed with %s (%d).\n", \
                        #call, __LINE__, __FILE__, cudaGetErrorString(cudaStatus), cudaStatus);    \
}
#endif //CUDA_RT_CALL

#include <cuComplex.h>

#include <nvToolsExt.h>

#include "pgm_io.h"

const float sharpening_filter[3][3] = {  
   { 0.0f,  -0.25f, 0.0f   },
   {-0.25f,  1.0f,  -0.25f },
   { 0.0f,  -0.25f, 0.0f   }
};
const int sharpening_filter_columns = sizeof(sharpening_filter[0])/sizeof(float);
const int sharpening_filter_rows = sizeof(sharpening_filter)/sizeof(sharpening_filter[0]);

//from http://homepages.inf.ed.ac.uk/rbf/HIPR2/gsmooth.htm 
const float gaussian_blur_filter[5][5] = {
   { 1.0f/273.0f,   4.0f/273.0f,  7.0f/273.0f,  4.0f/273.0f, 1.0f/273.0f },
   { 4.0f/273.0f,  16.0f/273.0f, 26.0f/273.0f, 16.0f/273.0f, 4.0f/273.0f },
   { 7.0f/273.0f,  26.0f/273.0f, 41.0f/273.0f, 26.0f/273.0f, 7.0f/273.0f },
   { 4.0f/273.0f,  16.0f/273.0f, 26.0f/273.0f, 16.0f/273.0f, 4.0f/273.0f },
   { 1.0f/273.0f,   4.0f/273.0f,  7.0f/273.0f,  4.0f/273.0f, 1.0f/273.0f }
};
const int gaussian_blur_filter_columns = sizeof(gaussian_blur_filter[0])/sizeof(float);
const int gaussian_blur_filter_rows = sizeof(gaussian_blur_filter)/sizeof(gaussian_blur_filter[0]);

__global__ void appy_filter(       cufftComplex * __restrict__ const picture_freq,
                             const cufftComplex * __restrict__ const combined_filter_freq,
                             const int length )
{
    const int i = blockIdx.x * blockDim.x + threadIdx.x;
    if ( i < length )
    {
        picture_freq[i] = cuCmulf( picture_freq[i], combined_filter_freq[i] );
    }
}

__global__ void normalize_output(       float * __restrict__ const picture_real,
                                  const float scale_factor,
                                  const int length )
{
    const int i = blockIdx.x * blockDim.x + threadIdx.x;
    if ( i < length )
    {
        picture_real[i] *= scale_factor;
    }
}

int main(int argc, char *argv[])
{
    if ( 3 != argc )
    {
        fprintf(stderr, "Usage: %s input.pgm output.pgm\n", argv[0]);
        return 1;
    }

    int width = 0;
    int height = 0;
    if ( 0 != read_pgm_file_header( &width,  &height, argv[1] ) )
    {
        fprintf(stderr, "ERROR: reading header of input from %s failed.\n", argv[1]);
        return 1;
    }
    
    //TODO: allocate picture_real in pinned host memory with cudaMallocHost
    float * const picture_real = (float*) malloc( height*width*sizeof(float) );
    float * picture_real_d;
    CUDA_RT_CALL( cudaMalloc( &picture_real_d, height*width*sizeof(float) ) );
    float * const sharpening_filter_real = (float*) malloc(width*height*sizeof(float));
    float * const gaussian_blur_filter_real = (float*) malloc(width*height*sizeof(float));
    const int width_in_freq_domain = (width/2+1);
    cufftComplex * const sharpening_filter_freq = (cufftComplex*) malloc( height*width_in_freq_domain*sizeof(cufftComplex) );
    cufftComplex * const gaussian_blur_filter_freq = (cufftComplex*) malloc( height*width_in_freq_domain*sizeof(cufftComplex) );
    cufftComplex * const combined_filter_freq = (cufftComplex*) malloc( height*width_in_freq_domain*sizeof(cufftComplex) );
    cufftComplex * combined_filter_freq_d;
    CUDA_RT_CALL( cudaMalloc( &combined_filter_freq_d, height*width_in_freq_domain*sizeof(cufftComplex) ) );
    
    cufftComplex * picture_freq_d;
    CUDA_RT_CALL( cudaMalloc( &picture_freq_d, height*width_in_freq_domain*sizeof(cufftComplex) ) );
    
    //create FFT plans
    fftwf_plan sharpening_filter_plan = fftwf_plan_dft_r2c_2d( height,width, sharpening_filter_real, (fftwf_complex*) sharpening_filter_freq, FFTW_ESTIMATE );
    fftwf_plan gaussian_blur_filter_plan = fftwf_plan_dft_r2c_2d( height,width, gaussian_blur_filter_real, (fftwf_complex*) gaussian_blur_filter_freq, FFTW_ESTIMATE );
    cufftHandle r2c_plan;
    cufftHandle c2r_plan;
    CUFFT_CALL( cufftPlan2d(&r2c_plan, height,width, CUFFT_R2C) );
    CUFFT_CALL( cufftPlan2d(&c2r_plan, height,width, CUFFT_C2R) );
    
    for (int row=0; row<height; ++row) {
        for (int column=0; column<width; column++) {
            if ( row < sharpening_filter_rows && column < sharpening_filter_columns )
                sharpening_filter_real[row*width+column] = sharpening_filter[row][column];
            else
                sharpening_filter_real[row*width + column] = 0.0f;
            if ( row < gaussian_blur_filter_rows && column < gaussian_blur_filter_columns )
                gaussian_blur_filter_real[row*width+column] = gaussian_blur_filter[row][column];
            else
                gaussian_blur_filter_real[row*width + column] = 0.0f;
        }
    }
     
    fftwf_execute_dft_r2c(sharpening_filter_plan,sharpening_filter_real,(fftwf_complex*) sharpening_filter_freq);
    fftwf_execute_dft_r2c(gaussian_blur_filter_plan,gaussian_blur_filter_real,(fftwf_complex*) gaussian_blur_filter_freq);

    //combine filters
    for (int row=0; row<height; ++row) {
        for (int column=0; column<width_in_freq_domain; ++column) {
            const cufftComplex sharpening_filter_freq_val = sharpening_filter_freq[row*width_in_freq_domain + column];
            const cufftComplex gaussian_blur_filter_freq_val = gaussian_blur_filter_freq[row*width_in_freq_domain + column];
            combined_filter_freq[row*width_in_freq_domain + column] = cuCmulf( sharpening_filter_freq_val, gaussian_blur_filter_freq_val );
        }
    }
    CUDA_RT_CALL( cudaMemcpy( combined_filter_freq_d, combined_filter_freq, height*width_in_freq_domain*sizeof(cufftComplex), cudaMemcpyHostToDevice ) );
    
    int error_code = 0;
    if ( 0 == read_pgm_file( picture_real, width,  height, argv[1] ) )
    {
        nvtxRangePushA("apply image filters");
        CUDA_RT_CALL( cudaMemcpy( picture_real_d, picture_real, height*width*sizeof(float), cudaMemcpyHostToDevice ) );
        CUFFT_CALL( cufftExecR2C(r2c_plan, picture_real_d, picture_freq_d) );
        
        const int length_in_freq_domain = height*width_in_freq_domain;
        appy_filter<<<((length_in_freq_domain-1)/128)+1,128>>>( picture_freq_d, combined_filter_freq_d, length_in_freq_domain );
        CUDA_RT_CALL( cudaGetLastError() );

        CUFFT_CALL( cufftExecC2R(c2r_plan, picture_freq_d, picture_real_d) );
        
        const float scale_factor = 1.0f/(height*width);
        const int length = height*width;
        normalize_output<<<((length-1)/128)+1,128>>>( picture_real_d, scale_factor, length );
        CUDA_RT_CALL( cudaGetLastError() );
        
        CUDA_RT_CALL( cudaMemcpy( picture_real, picture_real_d, height*width*sizeof(float), cudaMemcpyDeviceToHost ) );
        nvtxRangePop();
        
        if ( 0 != write_pgm_file( argv[2], width, height, picture_real ) )
        {
            fprintf(stderr, "ERROR: writing output to %s failed.\n", argv[2]);
            error_code = 1;
        }
        else
        {
            printf("Wrote output image %s of size %d x %d\n", argv[2], width, height );
        }
    }
        
    //destroy plans
    CUFFT_CALL( cufftDestroy(c2r_plan) );
    CUFFT_CALL( cufftDestroy(r2c_plan) );
    fftwf_destroy_plan(gaussian_blur_filter_plan);
    fftwf_destroy_plan(sharpening_filter_plan);
    
    CUDA_RT_CALL( cudaFree( picture_freq_d ) );
    CUDA_RT_CALL( cudaFree( combined_filter_freq_d ) );
    free( combined_filter_freq );
    free( gaussian_blur_filter_freq );
    free( sharpening_filter_freq );
    free( gaussian_blur_filter_real );
    free( sharpening_filter_real );
    CUDA_RT_CALL( cudaFree( picture_real_d ) );
    //TODO: free picture_real with cudaFreeHost
    free( picture_real );
    return error_code;
}
