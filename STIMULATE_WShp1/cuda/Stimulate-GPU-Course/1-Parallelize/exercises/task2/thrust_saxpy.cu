// Copyright (c) 2017-2018, NVIDIA CORPORATION. All rights reserved.

#include <thrust/transform.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/functional.h>
#include <iostream>



using namespace thrust::placeholders;


//
// struct saxpy_functor : public thrust::binary_function<...>
// {


//     saxpy_functor(...) : ...{}

//     ... operator()(...) const { 
//             return ...
//         }
// };

void saxpy_fast2(float A, thrust::device_vector<float>& X, thrust::device_vector<float>& Y)
{
    //TODO: implement later using thrust::binary_function https://thrust.github.io/doc/structthrust_1_1binary__function.html
    // use above struct as a starting poiny 
}


void saxpy_fast(float A, thrust::device_vector<float>& X, thrust::device_vector<float>& Y)
{
    //TODO: implement later using place holders, see https://thrust.github.io/doc/namespaceplaceholders.html
}

void saxpy_slow(float A, thrust::device_vector<float>& X, thrust::device_vector<float>& Y)
{
   //TODO: implement later, use a comniabtion of thrust::fill and
  // thrust::transform with thrust::plus and thrust::multiplies, you may need a temporary location

    // create temp

    // temp <- A
    
    // temp <- A * X
   
    // Y <- A * X + Y

}

int main(void)
{
   
        constexpr auto N=1<<10;
        const float a = 2.0;

        // host arrays
        // you could also start from std::vector or plain C arrays here
        thrust::host_vector<float> h_x(N);
        thrust::host_vector<float> h_y(N);

        //TODO: initialize h_x to 1.0, h_y to 10.0 using thrust::fill


        // since we have multiple implementations here keep the y vectors
        thrust::host_vector<float> h_y_init = h_y;

        std::cout << "Initial values in h_x and h_y" << std::endl;
        for(int i=0; i< 5; i++){
            std::cout << h_x[i] << "\t" << h_y[i] << std::endl;

        }

        //TODO: Define device copies of h_x and h_y 


        // slow variant
    std::cout << "Caling basic thrust version ..." << std::endl;
    {   

        //TODO: transfer h_y_initand h_x to their device copies
        // we use h_y_init here to initalize always with the default values as h_y gets overwritten with the results later
 

        //TODO: implement and call slow method
        //saxpy_slow(...);

        //TODO:  transfer to device values of y back to h_y
        
        
        // output
        std::cout << "Result values in h_x and h_y" << std::endl;
        for(int i=0; i< 5; i++){
            std::cout << h_x[i] << "\t" << h_y[i] << std::endl;
        }

    }

    // placeholder variant
    std::cout << "Caling thrust placeholder version ..." << std::endl;
    {
        //TODO: transfer to device
 

        //TODO:implement placeholder variant above
        //saxpy_fast(...);

        //TODO:  transfer to host
 
        std::cout << "Result values in h_x and h_y" << std::endl;
        for(int i=0; i< 5; i++){
            std::cout << h_x[i] << "\t" << h_y[i] << std::endl;
        }
    }
    

    // functor variant
    std::cout << "Caling thrust functor version ..." << std::endl;
    {
        //TODO: transfer to device


        //TODO: implement functor variant
        //saxpy_fast2();

        //TODO: transfer to host
      
        std::cout << "Result values in h_x and h_y" << std::endl;
        for(int i=0; i< 5; i++){
            std::cout << h_x[i] << "\t" << h_y[i] << std::endl;
        }
    }

    return 0;
}

