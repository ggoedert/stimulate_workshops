// Copyright (c) 2017-2018, NVIDIA CORPORATION. All rights reserved.
#include <stdio.h>

#ifndef CUDA_RT_CALL
#define CUDA_RT_CALL( call )                                                                       \
{                                                                                                  \
    cudaError_t cudaStatus = call;                                                                 \
    if ( cudaSuccess != cudaStatus )                                                               \
        fprintf(stderr, "ERROR: CUDA RT call \"%s\" in line %d of file %s failed with %s (%d).\n", \
                        #call, __LINE__, __FILE__, cudaGetErrorString(cudaStatus), cudaStatus);    \
}
#endif //CUDA_RT_CALL

// cpu saxpy
void saxpy(float a, float* x, float* y, int n)
{
  for (int i = 0; i < n; ++i)
      y[i] = a*x[i] + y[i];
}


//cuda saxpy gridstride
// cuda saxpy
__global__
void cuda_saxpy_1block1thread(float a, float* x, float* y, int n)
{
//TODO: Implement later (see below)
}

__global__
void cuda_saxpy_1thread(float a, float* x, float* y, int n)
{
//TODO: Implement later (see below)
}


__global__
void cuda_saxpy_1block(float a, float* x, float* y, int n)
{
//TODO: Implement later (see below)
}

// cuda saxpy
__global__
void cuda_saxpy(float a, float* x, float* y, int n)
{
//TODO: Implement later (see below)
}

// cuda saxpy - gridstride
void cuda_saxpy_gridstride(float a, float* x, float* y, int n)
{
//TODO: Implement later (see below)
// see: https://devblogs.nvidia.com/cuda-pro-tip-write-flexible-kernels-grid-stride-loops/    
}

int main(int argc, char** argv){

    
    
    const int N = 1<<20;
    float a = 2.0;

    float* x = (float*) malloc(N * sizeof(float));
    float* y = (float*) malloc(N * sizeof(float));
    
    // set x and y to some initial values
    for (int i = 0; i < N; ++i) x[i] = 1.0;
    for (int i = 0; i < N; ++i) y[i] = 10.0;

    // store a copy of inital values for reuse
    float* y_init = (float*) malloc(N * sizeof(float));
    memcpy(y_init, y, N*sizeof(float));

    

    printf("Initial values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }


    // CPU Version
    printf("Calling CPU version ...\n");
    {
    saxpy(a, x, y, N);
   
    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }

    }

    // restore y for next version
    memcpy(y, y_init, N * sizeof(float));


    //TODO: add device pointers for x and y and allocate memory on the device device pointers
    //Wrap CUDA Runtime calls in CUDA_RT_CALL(...)
    float* d_x;
    float* d_y;
    //TODO: Allocate d_x and d_y on the device

    
    // Version with 1 thread
    printf("Calling GPU version with only 1 thread ...\n");
    {
    //TODO: Copy x,y to device 
    
    // TODO: Implement the kernel above using just 1 block with 1024 threads on the device.
    // Complete the call
    // cuda_saxpy_1block1thread<<<...>>>(...);
    
    // TODO: enable the following call to cudaGetLastError
    //CUDA_RT_CALL(cudaGetLastError());

    //TODO: Copy d_y back to y
   
    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }

    }

    // restore y for next version
    memcpy(y, y_init, N * sizeof(float));

    // Version with 1 block, multiple threads
    printf("Calling GPU version with only 1 block, multiple threads ...\n");
    {
    //TODO: Copy x,y to device 
    
    // size of the block is limited to 1024 threads, we still need the loop in the kernel
    // see https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#compute-capabilities
    //int blockSize = 1024;
    
    // TODO: Implement the kernel above using just 1 block with 1024 threads on the device.
    // Complete the call
    //cuda_saxpy_1block<<<1, blockSize>>>(...;
   // CUDA_RT_CALL(cudaGetLastError());

    //TODO: Copy d_y back to y
   

    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }

    }

    // restore y for next version
    memcpy(y, y_init, N * sizeof(float));

    // Version with multiple blocks, 1 thread per block
    printf("Calling GPU version with multiple blocks, 1 thread per block ...\n");
    {
    //TODO: Copy x,y to device 

    
    // TODO: Define the exectution configuration, use N blocks with 1 thread each
 
    
    //TODO: Call the saxpy kernel
    //cuda_saxpy_1thread<<<...>>>(...);
    //CUDA_RT_CALL(cudaGetLastError());

    //TODO: Copy d_y back to y
   

    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }

    }

    // restore y for next version
    memcpy(y, y_init, N * sizeof(float));
    
    // Version with multiple block, 128 thread per block
    printf("Calling GPU version with 128 threads per block, as many blocks as needed ...\n");
    {
    //TODO: Copy x,y to device 
    

    //TODO: Define the exectution configuration, i.e. blockSize and gridSize
    // 

    
    // TODO: Call the saxpy kernel
    //cuda_saxpy<<<...>>>(...);
    
    //TODO: Copy d_y back to y
    

    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }

    }


    // restore y for next version
    memcpy(y, y_init, N * sizeof(float));
    // Version with grid-strided loop, fixed amount of blocks
    printf("Calling GPU version with grid strided loop ...\n");
    {
    //TODO: Copy x,y to device, call the kernel with blocksize and gridsize, copy data back
    
    // Define the exectution configuration, i.e. blockSize and gridSize
    int blockSize = 384; 
    int gridSize = 26; 
    

    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }

    }

    //TODO: Free device pointers 

    free(x);
    free(y);

}
