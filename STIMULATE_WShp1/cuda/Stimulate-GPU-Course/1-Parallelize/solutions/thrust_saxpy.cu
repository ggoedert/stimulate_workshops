// Copyright (c) 2017-2018, NVIDIA CORPORATION. All rights reserved.

#include <thrust/transform.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/functional.h>
#include <iostream>



using namespace thrust::placeholders;



struct saxpy_functor : public thrust::binary_function<float,float,float>
{
    const float a;

    saxpy_functor(float _a) : a(_a) {}

    __host__ __device__
        float operator()(const float& x, const float& y) const { 
            return a * x + y;
        }
};

void saxpy_fast2(float A, thrust::device_vector<float>& X, thrust::device_vector<float>& Y)
{
    // Y <- A * X + Y
    thrust::transform(X.begin(), X.end(), Y.begin(), Y.begin(), saxpy_functor(A));
}


void saxpy_fast(float A, thrust::device_vector<float>& X, thrust::device_vector<float>& Y)
{
    // Y <- A * X + Y
    thrust::transform(X.begin(), X.end(), Y.begin(), Y.begin(), A * _1 + _2);
}

void saxpy_slow(float A, thrust::device_vector<float>& X, thrust::device_vector<float>& Y)
{
    thrust::device_vector<float> temp(X.size());
   
    // temp <- A
    thrust::fill(temp.begin(), temp.end(), A);
    
    // temp <- A * X
    thrust::transform(X.begin(), X.end(), temp.begin(), temp.begin(), thrust::multiplies<float>());

    // Y <- A * X + Y
    thrust::transform(temp.begin(), temp.end(), Y.begin(), Y.begin(), thrust::plus<float>());
}

int main(void)
{
   
        constexpr auto N=1<<10;
        const float a = 2.0;

        // host arrays
        // you could also start from std::vector or plain C arrays here
        thrust::host_vector<float> h_x(N);
        thrust::host_vector<float> h_y(N);

        // initialize
        thrust::fill(h_x.begin(), h_x.end(), 1.0);
        thrust::fill(h_y.begin(), h_y.end(), 10.0);

        // since we have multiple implementations here keep the y vectors
        thrust::host_vector<float> h_y_init = h_y;

        std::cout << "Initial values in h_x and h_y" << std::endl;
        for(int i=0; i< 5; i++){
            std::cout << h_x[i] << "\t" << h_y[i] << std::endl;

        }

        // device vectors
        thrust::device_vector<float> d_x;
        thrust::device_vector<float> d_y;


        // slow variant
    {   

        // transfer to device
        d_x = h_x;
        d_y = h_y_init;

        // slow method
        saxpy_slow(a, d_x, d_y);

        // transfer to host
        h_y = d_y;
        
        // output
        std::cout << "Result values in h_x and h_y" << std::endl;
        for(int i=0; i< 5; i++){
            std::cout << h_x[i] << "\t" << h_y[i] << std::endl;
        }

    }

    // placeholder variant
    std::cout << "Caling thrust placeholder version ..." << std::endl;
    {
        // transfer to device
        d_x = h_x;
        d_y = h_y_init;

        // fast method
        saxpy_fast(a, d_x, d_y);

        // transfer to host
         h_y = d_y;
        std::cout << "Result values in h_x and h_y" << std::endl;
        for(int i=0; i< 5; i++){
            std::cout << h_x[i] << "\t" << h_y[i] << std::endl;
        }
    }
    

    // functor variant
    std::cout << "Caling thrust functor version ..." << std::endl;
    {
        // transfer to device
        d_x = h_x;
        d_y = h_y_init;

        // fast method
        saxpy_fast2(a, d_x, d_y);

        // transfer to host
        h_y = d_y;
        std::cout << "Result values in h_x and h_y" << std::endl;
        for(int i=0; i< 5; i++){
            std::cout << h_x[i] << "\t" << h_y[i] << std::endl;
        }
    }

    return 0;
}

