// Copyright (c) 2017-2018, NVIDIA CORPORATION. All rights reserved.

#include <stdio.h>
#include <stdlib.h>

#include <cuda_runtime.h>
#include "cublas_v2.h"

#ifndef CUDA_RT_CALL
#define CUDA_RT_CALL( call )                                                                       \
{                                                                                                  \
    cudaError_t cudaStatus = call;                                                                 \
    if ( cudaSuccess != cudaStatus )                                                               \
        fprintf(stderr, "ERROR: CUDA RT call \"%s\" in line %d of file %s failed with %s (%d).\n", \
                        #call, __LINE__, __FILE__, cudaGetErrorString(cudaStatus), cudaStatus);    \
}
#endif //CUDA_RT_CALL


void saxpy(float a, float* x, float* y, int n)
{
  for (int i = 0; i < n; ++i)
      y[i] = a*x[i] + y[i];
}



int main(int argc, char** argv){

    
    
    const int N = 1<<20;
    float a = 2.0;

    float* x = (float*) malloc(N * sizeof(float));
    float* y = (float*) malloc(N * sizeof(float));
    
    // set x and y to some initial values
    for (int i = 0; i < N; ++i) x[i] = 1.0;
    for (int i = 0; i < N; ++i) y[i] = 10.0;
    

    printf("Initial values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }


    // initialize cublas
    cublasHandle_t handle;
    cublasStatus_t status = cublasCreate(&handle);

    if (status != CUBLAS_STATUS_SUCCESS) {
        fprintf(stderr, "ERROR: CUBLAS initialization error\n");
    return -1;
    }
    
    // device pointers
    float* d_x;
    float* d_y;
    // Allocate device memory
    CUDA_RT_CALL( cudaMalloc((void**)&d_x, N * sizeof(float)));
    CUDA_RT_CALL( cudaMalloc((void**)&d_y, N * sizeof(float)));
    
    /* Set the device vectors with values from the host vectors */
    status = cublasSetVector(N, sizeof(float), x, 1, d_x, 1);
    if (status != CUBLAS_STATUS_SUCCESS) {
        fprintf(stderr, "ERROR: CUBLAS SetVector (x)\n");
        return -1;
    }

    status = cublasSetVector(N, sizeof(float), y, 1, d_y, 1);
    if (status != CUBLAS_STATUS_SUCCESS) {
        fprintf(stderr, "ERROR: CUBLAS SetVector (y)\n");
        return -1;
    }   

       
    // Call cublas
    status = cublasSaxpy(handle, N, &a, d_x, 1, d_y, 1);
    if (status != CUBLAS_STATUS_SUCCESS) {
        fprintf(stderr, "ERROR: CUBLAS Saxpy\n");
        return -1;
    } 

    // Get the result vector back from the device
    status = cublasGetVector(N, sizeof(float), d_y, 1, y, 1);
    if (status != CUBLAS_STATUS_SUCCESS) {
        fprintf(stderr, "ERROR: CUBLAS GetVector (y)\n");
        return -1;
    }     


    // shutdown cublas
    status = cublasDestroy(handle);
    if (status != CUBLAS_STATUS_SUCCESS) {
        fprintf(stderr, "ERROR: CUBLAS shutdown error\n");
        return -1;
    }
    // Free device pointers 
    CUDA_RT_CALL(cudaFree(d_x));
    CUDA_RT_CALL(cudaFree(d_y));

    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }


    free(x);
    free(y);

}
