/* Copyright (c) 2017-2018, NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cassert>
#include <complex>

#include <fftw3.h>

//TODO 1: include nvToolsExt.h https://devblogs.nvidia.com/cuda-pro-tip-generate-custom-application-profile-timelines-nvtx/

#include "pgm_io.h"

const float sharpening_filter[3][3] = {  
   { 0.0f,  -0.25f, 0.0f   },
   {-0.25f,  1.0f,  -0.25f },
   { 0.0f,  -0.25f, 0.0f   }
};
const int sharpening_filter_columns = sizeof(sharpening_filter[0])/sizeof(float);
const int sharpening_filter_rows = sizeof(sharpening_filter)/sizeof(sharpening_filter[0]);

//from http://homepages.inf.ed.ac.uk/rbf/HIPR2/gsmooth.htm 
const float gaussian_blur_filter[5][5] = {
   { 1.0f/273.0f,   4.0f/273.0f,  7.0f/273.0f,  4.0f/273.0f, 1.0f/273.0f },
   { 4.0f/273.0f,  16.0f/273.0f, 26.0f/273.0f, 16.0f/273.0f, 4.0f/273.0f },
   { 7.0f/273.0f,  26.0f/273.0f, 41.0f/273.0f, 26.0f/273.0f, 7.0f/273.0f },
   { 4.0f/273.0f,  16.0f/273.0f, 26.0f/273.0f, 16.0f/273.0f, 4.0f/273.0f },
   { 1.0f/273.0f,   4.0f/273.0f,  7.0f/273.0f,  4.0f/273.0f, 1.0f/273.0f }
};
const int gaussian_blur_filter_columns = sizeof(gaussian_blur_filter[0])/sizeof(float);
const int gaussian_blur_filter_rows = sizeof(gaussian_blur_filter)/sizeof(gaussian_blur_filter[0]);

int main(int argc, char *argv[])
{
    if ( 3 != argc )
    {
        fprintf(stderr, "Usage: %s input.pgm output.pgm\n", argv[0]);
        return 1;
    }

    int width = 0;
    int height = 0;
    if ( 0 != read_pgm_file_header( &width,  &height, argv[1] ) )
    {
        fprintf(stderr, "ERROR: reading header of input from %s failed.\n", argv[1]);
        return 1;
    }
    
    float * const picture_real = (float*) malloc( height*width*sizeof(float) );
    float * const sharpening_filter_real = (float*) malloc(width*height*sizeof(float));
    float * const gaussian_blur_filter_real = (float*) malloc(width*height*sizeof(float));
    const int width_in_freq_domain = (width/2+1);
    std::complex<float> * const sharpening_filter_freq = (std::complex<float>*) malloc( height*width_in_freq_domain*sizeof(std::complex<float>) );
    std::complex<float> * const gaussian_blur_filter_freq = (std::complex<float>*) malloc( height*width_in_freq_domain*sizeof(std::complex<float>) );
    std::complex<float> * const combined_filter_freq = (std::complex<float>*) malloc( height*width_in_freq_domain*sizeof(std::complex<float>) );
    std::complex<float> * const picture_freq = (std::complex<float>*) malloc( height*width_in_freq_domain*sizeof(std::complex<float>) );
    
    //create FFT plans
    fftwf_plan sharpening_filter_plan = fftwf_plan_dft_r2c_2d( height,width, sharpening_filter_real, (fftwf_complex*) sharpening_filter_freq, FFTW_ESTIMATE );
    fftwf_plan gaussian_blur_filter_plan = fftwf_plan_dft_r2c_2d( height,width, gaussian_blur_filter_real, (fftwf_complex*) gaussian_blur_filter_freq, FFTW_ESTIMATE );
    fftwf_plan r2c_picture_plan = fftwf_plan_dft_r2c_2d( height,width, picture_real, (fftwf_complex*) picture_freq, FFTW_ESTIMATE );
    fftwf_plan c2r_picture_plan = fftwf_plan_dft_c2r_2d( height,width, (fftwf_complex*) picture_freq, picture_real, FFTW_ESTIMATE );
    
    for (int row=0; row<height; ++row) {
        for (int column=0; column<width; column++) {
            if ( row < sharpening_filter_rows && column < sharpening_filter_columns )
                sharpening_filter_real[row*width+column] = sharpening_filter[row][column];
            else
                sharpening_filter_real[row*width + column] = 0.0f;
            if ( row < gaussian_blur_filter_rows && column < gaussian_blur_filter_columns )
                gaussian_blur_filter_real[row*width+column] = gaussian_blur_filter[row][column];
            else
                gaussian_blur_filter_real[row*width + column] = 0.0f;
        }
    }
     
    fftwf_execute_dft_r2c(sharpening_filter_plan,sharpening_filter_real,(fftwf_complex*) sharpening_filter_freq);
    fftwf_execute_dft_r2c(gaussian_blur_filter_plan,gaussian_blur_filter_real,(fftwf_complex*) gaussian_blur_filter_freq);

    //combine filters
    for (int row=0; row<height; ++row) {
        for (int column=0; column<width_in_freq_domain; ++column) {
            const std::complex<float> sharpening_filter_freq_val = sharpening_filter_freq[row*width_in_freq_domain + column];
            const std::complex<float> gaussian_blur_filter_freq_val = gaussian_blur_filter_freq[row*width_in_freq_domain + column];
            combined_filter_freq[row*width_in_freq_domain + column] = sharpening_filter_freq_val*gaussian_blur_filter_freq_val;
        }
    }
    
    int error_code = 0;
    if ( 0 == read_pgm_file( picture_real, width,  height, argv[1] ) )
    {
        //TODO 1: push NVTX range
        fftwf_execute_dft_r2c(r2c_picture_plan,picture_real, (fftwf_complex*) picture_freq);
        
        //apply filter
        for (int row=0; row<height; ++row) {
            for (int column=0; column<width_in_freq_domain; ++column) {
                picture_freq[row*width_in_freq_domain + column] *= combined_filter_freq[row*width_in_freq_domain + column];
            }
        }

        fftwf_execute_dft_c2r(c2r_picture_plan,(fftwf_complex*) picture_freq,picture_real);
        
        //normalize output
        const float scale_factor = 1.0f/(height*width);
        for (int row=0; row<height; ++row) {
            for (int column=0; column<width; column++) {
                picture_real[row*width + column] *= scale_factor;
            }
        }
        //TODO 1: pop NVTX range
        
        if ( 0 != write_pgm_file( argv[2], width, height, picture_real ) )
        {
            fprintf(stderr, "ERROR: writing output to %s failed.\n", argv[2]);
            error_code = 1;
        }
        else
        {
            printf("Wrote output image %s of size %d x %d\n", argv[2], width, height );
        }
    }
        
    //destroy plans
    fftwf_destroy_plan(c2r_picture_plan);
    fftwf_destroy_plan(r2c_picture_plan);
    fftwf_destroy_plan(gaussian_blur_filter_plan);
    fftwf_destroy_plan(sharpening_filter_plan);
    
    free( picture_freq );
    free( combined_filter_freq );
    free( gaussian_blur_filter_freq );
    free( sharpening_filter_freq );
    free( gaussian_blur_filter_real );
    free( sharpening_filter_real );
    free( picture_real);
    return error_code;
}
