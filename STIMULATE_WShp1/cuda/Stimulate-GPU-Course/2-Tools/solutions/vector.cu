// Copyright (c) 2017-2018, NVIDIA CORPORATION. All rights reserved.
#include <stdio.h>

#ifndef CUDA_RT_CALL
#define CUDA_RT_CALL( call )                                                                       \
{                                                                                                  \
    cudaError_t cudaStatus = call;                                                                 \
    if ( cudaSuccess != cudaStatus )                                                               \
        fprintf(stderr, "ERROR: CUDA RT call \"%s\" in line %d of file %s failed with %s (%d).\n", \
                        #call, __LINE__, __FILE__, cudaGetErrorString(cudaStatus), cudaStatus);    \
}
#endif //CUDA_RT_CALL

// kernel
__global__
void setvec(float a, float* x, int n)
{
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < n) x[i] = a;
}



int main(int argc, char** argv){

    
    
    const int N = 4096;
    float a = 2.0;

    
    // device pointers
    float* d_x;
    // Allocate d_x and d_y on the device
    CUDA_RT_CALL( cudaMalloc(&d_x, N * sizeof(float)));
    
    // Define the exectution configuration, i.e. blockSize and gridSize
    int blockSize = 128;
    int gridSize = (N+blockSize-1)/blockSize;
    
    // Call the saxpy kernel
    setvec<<<gridSize, blockSize>>>(a, d_x, N);
    CUDA_RT_CALL(cudaGetLastError());
    CUDA_RT_CALL(cudaDeviceSynchronize());


    // Free device pointers 
    CUDA_RT_CALL(cudaFree(d_x));


}
