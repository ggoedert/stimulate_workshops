// Copyright (c) 2017-2018, NVIDIA CORPORATION. All rights reserved.
#include <stdio.h>

#ifndef CUDA_RT_CALL
#define CUDA_RT_CALL( call )                                                                       \
{                                                                                                  \
    cudaError_t cudaStatus = call;                                                                 \
    if ( cudaSuccess != cudaStatus )                                                               \
        fprintf(stderr, "ERROR: CUDA RT call \"%s\" in line %d of file %s failed with %s (%d).\n", \
                        #call, __LINE__, __FILE__, cudaGetErrorString(cudaStatus), cudaStatus);    \
}
#endif //CUDA_RT_CALL

// cpu saxpy
void saxpy(float a, float* x, float* y, int n)
{
  for (int i = 0; i < n; ++i)
      y[i] = a*x[i] + y[i];
}


//cuda saxpy gridstride
// cuda saxpy
__global__
void cuda_saxpy_1block1thread(float a, float* x, float* y, int n)
{
  for(int i = 0; i < n; i++)
    y[i] = a*x[i] + y[i];
}

__global__
void cuda_saxpy_1thread(float a, float* x, float* y, int n)
{
  for(int i = blockIdx.x; i < n; i += gridDim.x)
    y[i] = a*x[i] + y[i];
}


__global__
void cuda_saxpy_1block(float a, float* x, float* y, int n)
{
  for(int i = threadIdx.x; i<n; i += blockDim.x)
    y[i] = a*x[i] + y[i];
}

// cuda saxpy
__global__
void cuda_saxpy(float a, float* x, float* y, int n)
{
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < n) y[i] = a*x[i] + y[i];
}

// cuda saxpy - gridstride, see https://devblogs.nvidia.com/cuda-pro-tip-write-flexible-kernels-grid-stride-loops/
__global__
void cuda_saxpy_gridstride(float a, float* x, float* y, int n)
{
    for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < n; i += blockDim.x * gridDim.x)
        y[i] = a*x[i] + y[i];
}

int main(int argc, char** argv){

    
    
    const int N = 1<<20;
    float a = 2.0;

    float* x = (float*) malloc(N * sizeof(float));
    float* y = (float*) malloc(N * sizeof(float));
    
    // set x and y to some initial values
    for (int i = 0; i < N; ++i) x[i] = 1.0;
    for (int i = 0; i < N; ++i) y[i] = 10.0;

    // store a copy of inital values for reuse
    float* y_init = (float*) malloc(N * sizeof(float));
    memcpy(y_init, y, N*sizeof(float));

    

    printf("Initial values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }

    // CPU Version
    printf("Calling CPU version ...\n");
    {
    saxpy(a, x, y, N);
   
    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }

    }

    // restore y for next version
    memcpy(y, y_init, N * sizeof(float));

    // device pointers
    float* d_x;
    float* d_y;
    // Allocate d_x and d_y on the device
    CUDA_RT_CALL( cudaMalloc(&d_x, N * sizeof(float)));
    CUDA_RT_CALL( cudaMalloc(&d_y, N * sizeof(float)));
    
    // Version with 1 thread
    printf("Calling GPU version with only 1 thread ...\n");
    {
    // Copy x,y to device 
    CUDA_RT_CALL( cudaMemcpy(d_x, x, N * sizeof(float), cudaMemcpyHostToDevice));
    CUDA_RT_CALL( cudaMemcpy(d_y, y, N * sizeof(float), cudaMemcpyHostToDevice));
    
        // Call the saxpy kernel
    cuda_saxpy_1block1thread<<<1, 1>>>(a, d_x, d_y, N);
    CUDA_RT_CALL(cudaGetLastError());

    // Copy d_y back to y
    CUDA_RT_CALL(cudaMemcpy(y, d_y, N * sizeof(float), cudaMemcpyDeviceToHost));
   

    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }

    }

    // restore y for next version
    memcpy(y, y_init, N * sizeof(float));

    // Version with 1 block, multiple threads
    printf("Calling GPU version with only 1 block, multiple threads ...\n");
    {
    // Copy x,y to device 
    CUDA_RT_CALL( cudaMemcpy(d_x, x, N * sizeof(float), cudaMemcpyHostToDevice));
    CUDA_RT_CALL( cudaMemcpy(d_y, y, N * sizeof(float), cudaMemcpyHostToDevice));
    
    // size of the block is limited to 1024 threads, we still need the loop in the kernel
    // see https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#compute-capabilities
    int blockSize = 1024;
    
    // Call the saxpy kernel
    cuda_saxpy_1block<<<1, blockSize>>>(a, d_x, d_y, N);
    CUDA_RT_CALL(cudaGetLastError());

    // Copy d_y back to y
    CUDA_RT_CALL(cudaMemcpy(y, d_y, N * sizeof(float), cudaMemcpyDeviceToHost));
   

    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }

    }

    // restore y for next version
    memcpy(y, y_init, N * sizeof(float));

    // Version with multiple blocks, 1 thread per block
    printf("Calling GPU version with multiple blocks, 1 thread per block ...\n");
    {
    // Copy x,y to device 
    CUDA_RT_CALL( cudaMemcpy(d_x, x, N * sizeof(float), cudaMemcpyHostToDevice));
    CUDA_RT_CALL( cudaMemcpy(d_y, y, N * sizeof(float), cudaMemcpyHostToDevice));
    
    // Define the exectution configuration, use N blocks
    int gridSize = N;
    
    // Call the saxpy kernel
    cuda_saxpy_1thread<<<gridSize, 1>>>(a, d_x, d_y, N);
    CUDA_RT_CALL(cudaGetLastError());

    // Copy d_y back to y
    CUDA_RT_CALL(cudaMemcpy(y, d_y, N * sizeof(float), cudaMemcpyDeviceToHost));
   

    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }

    }

    // restore y for next version
    memcpy(y, y_init, N * sizeof(float));

    // Version with multiple block, 128 thread per block
    printf("Calling GPU version with 128 threads per block, as many blocks as needed ...\n");
    {
    // Copy x,y to device 
    CUDA_RT_CALL( cudaMemcpy(d_x, x, N * sizeof(float), cudaMemcpyHostToDevice));
    CUDA_RT_CALL( cudaMemcpy(d_y, y, N * sizeof(float), cudaMemcpyHostToDevice));
    
    // Define the exectution configuration, i.e. blockSize and gridSize
    int blockSize = 128;
    int gridSize = (N+blockSize-1)/blockSize;
    
    // Call the saxpy kernel
    cuda_saxpy<<<gridSize, blockSize>>>(a, d_x, d_y, N);
    CUDA_RT_CALL(cudaGetLastError());

    // Copy d_y back to y
    CUDA_RT_CALL(cudaMemcpy(y, d_y, N * sizeof(float), cudaMemcpyDeviceToHost));
   

    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }

    }


    // restore y for next version
    memcpy(y, y_init, N * sizeof(float));
    // Version with grid-strided loop, fixed amount of blocks
    printf("Calling GPU version with grid strided loop ...\n");
    {
    // Copy x,y to device 
    CUDA_RT_CALL( cudaMemcpy(d_x, x, N * sizeof(float), cudaMemcpyHostToDevice));
    CUDA_RT_CALL( cudaMemcpy(d_y, y, N * sizeof(float), cudaMemcpyHostToDevice));
    
    // Define the exectution configuration, i.e. blockSize and gridSize
    int blockSize = 384; 
    int gridSize = 26; 
    
    // Call the saxpy kernel
    cuda_saxpy<<<gridSize, blockSize>>>(a, d_x, d_y, N);
    CUDA_RT_CALL(cudaGetLastError());

    // Copy d_y back to y
    CUDA_RT_CALL(cudaMemcpy(y, d_y, N * sizeof(float), cudaMemcpyDeviceToHost));
   

    printf("Result values in x and y\n");
    printf("i\tx[i]\ty[i]\n");
        for(int i=0; i< 5; i++){
            printf("%i\t%f\t%f\n",i,x[i],y[i]);
        }

    }

    // Free device pointers 
    CUDA_RT_CALL(cudaFree(d_x));
    CUDA_RT_CALL(cudaFree(d_y));

    free(x);
    free(y);

}
