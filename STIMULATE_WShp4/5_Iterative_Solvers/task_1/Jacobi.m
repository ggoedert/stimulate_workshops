function [x , counter,residue] = Jacobi(A,b,eps,max_iter)

    counter = 0;
    x = ones(size(b));
    
    M = diag(diag(A));
    N = M - A;
    
    res = eps + 1;
    residue = zeros(max_iter,1);
    
    while (res > eps) && (counter < max_iter)
        
        x = M \ (N*x + b);
        res = norm(b - A*x);
        counter = counter + 1;
        residue(counter) = res;
        
    end

    residue(counter+1:end) = [];
    
end