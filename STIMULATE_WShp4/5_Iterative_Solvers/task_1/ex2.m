

eps = 1e-4;
max_iter = 1e5;
N = [4 , 8, 16, 32, 64, 128];

counter_j = zeros(size(N)); 
counter_g = zeros(size(N));

for i = 1:length(N) 
    
    A = discreteLaplacian(N(i));
    b = (1/(N(i)+1)^2)*ones(N(i)*N(i),1);

    [x , counter_j(i), ~] = Jacobi(A,b,eps,max_iter);
    [x , counter_g(i),~] = Gauss_Seidel(A,b,eps,max_iter);
    
    fprintf('For N = %d \n',N(i))
    fprintf('--Jacobi iter : %d \n',counter_j(i))
    fprintf('--Gauss-Seidel iter : %d \n',counter_g(i))
    
    pause(0.1)
end

figure; hold on
plot(N,counter_j,'b*')
plot(N,counter_g,'r*')
legend('Jacobi','GS')
xlabel('N')
ylabel('# iterations')
title(sprintf('eps = %g',eps))