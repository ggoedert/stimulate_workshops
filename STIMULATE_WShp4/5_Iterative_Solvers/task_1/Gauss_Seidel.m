function [x , counter,residue] = Gauss_Seidel(A,b,eps,max_iter)

    counter = 0;
    x = ones(size(b));
    
    M = diag(diag(A)) + tril(A,-1);
    N = -triu(A,1);
    
    res = eps + 1;
    residue = zeros(max_iter,1);
    
    while (res > eps) && (counter < max_iter)
        
        x = M \ (N*x + b);
        res = norm(b - A*x);
        counter = counter + 1;
        residue(counter) = res;
        
    end

    residue(counter+1:end) = [];
    
end