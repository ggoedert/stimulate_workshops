

N = 64;
A = discreteLaplacian(N);
xsol = ones(N.^2,1);
b = A*xsol;

tol = 1E-10;
maxit = 1E3;

% Jacobi: diagonal scaling
[x,flag,relres,iter,resvec,xvec] = pcg_loc(A,b,tol,maxit,'jacobi',[],[]);

% Plot the convergence (relative residue);
figure;
subplot(1,2,1)
semilogy([0:iter],resvec);
xlabel('# iter (n)')
ylabel('ln(r_n)')

% Plot the error for each iteration
dif = xvec - xsol;
error2  = sqrt(diag(dif'*dif));
errorA  = sqrt(diag(dif'*(A*dif)));

subplot(1,2,2)
semilogy([0:iter],error2,'xb')
hold on;
semilogy([0:iter],errorA,'*r')
legend('|e|_2','|e|_A')
