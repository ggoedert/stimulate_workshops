function [ T, V ] = lanczos( A, b, maxit, x0)

n = size(A,1);

if nargin < 3
   maxit = n;
end

if maxit > n
   maxit = n;
end

if nargin < 4 || isempty(x0)
   x0 = zeros(n,1);
end

alpha = zeros(maxit,1);
beta = zeros(maxit+1,1);
V = zeros(n,maxit+1);

r = b - A*x0;
beta(1) = norm(r);
V(:,1) = r/beta(1);
for i = 1:maxit
   V(:,i+1) = A*V(:,i);
   if i > 1
      V(:,i+1) = V(:,i+1) - beta(i)*V(:,i-1);
   end
   alpha(i) = V(:,i)'*V(:,i+1);
   V(:,i+1) = V(:,i+1) - alpha(i)*V(:,i);
   beta(i+1) = norm(V(:,i+1));
   
   if beta(i+1) > 1e-10
      V(:,i+1) = V(:,i+1)/beta(i+1);
   else
      V(:,i+1) = zeros(n,1);
   end
end

T = spdiags([[beta;0] [alpha;0;0] [0;beta]],[-1 0 1],maxit+1,maxit);
