
N = 50;
tol = 1e-10;
maxit = 50;  
b1 = eye(N,1);
b2 = ones(N,1);

for restart = [4, 24, 25]
figure;
% A1
A1 = zeros(N,N);
A1(1:2,end-1:end) = eye(2);
A1(3:end,1:end-2) = eye(N-2);

[x11,flag11,relres11,iter11,resvec11] = gmres(A1,b1,restart,tol,maxit);
subplot(3,2,1)
semilogy(resvec11);
xlabel('# iter (n)')
ylabel('ln(r_n)')
title(sprintf('A1 and b1 = eye(N,1), restart = %d',restart))

[x12,flag12,relres12,iter12,resvec12] = gmres(A1,b2,restart,tol,maxit);
subplot(3,2,2)
semilogy(resvec12);
xlabel('# iter (n)')
ylabel('ln(r_n)')
title(sprintf('A1 and b2 = ones(N,1), restart = %d',restart))

% A2
A2 = diag(3.*ones(N,1),0) + diag(-ones(N-1,1),1)+ diag(-ones(N-2,1),2)+ diag(-ones(N-1,1),-1)+ diag(-ones(N-2,1),-2);
[x21,flag21,relres21,iter21,resvec21] = gmres(A2,b1,restart,tol,maxit);
subplot(3,2,3)
semilogy(resvec21);
xlabel('# iter (n)')
ylabel('ln(r_n)')
title(sprintf('A2 and b1 = eye(N,1), restart = %d',restart))

[x22,flag22,relres22,iter22,resvec22] = gmres(A2,b2,restart,tol,maxit);
subplot(3,2,4)
semilogy(resvec22);
xlabel('# iter (n)')
ylabel('ln(r_n)')
title(sprintf('A2 and b2 = ones(N,1), restart = %d',restart))

% A3
A3 = wilkinson(N);
[x31,flag31,relres31,iter31,resvec31] = gmres(A3,b1,restart,tol,maxit);
subplot(3,2,5)
semilogy(resvec31);
xlabel('# iter (n)')
ylabel('ln(r_n)')
title(sprintf('A3 and b2 = eye(N,1), restart = %d',restart))

[x32,flag32,relres32,iter32,resvec32] = gmres(A3,b2,restart,tol,maxit);
subplot(3,2,6)
semilogy(resvec32);
xlabel('# iter (n)')
ylabel('ln(r_n)')
title(sprintf('A3 and b2 = ones(N,1), restart = %d',restart))

end