% Preconditioned Spectrum
% Conjugate Gradients Polynomial

N = 16;
fprintf(1,'Running Poisson-Test on %dx%d grid\n',N,N);
A = discreteLaplacian(N);
m = size(A,1);
e = zeros(m,1);
e(N*(N+1)/2) = 1;
b = e;
beta = norm(b);
b = b/beta;
xstar = A\b;

ww = eig(A);

B = zeros(m);
opts = cell(2,1);
opts{1} = A;
opts{2} = 1.5;
for i = 1:m
  B(:,i) = ssor_step(A(:,i),A,1.5);
end
wwP = eig(B);

[ TSSOR, VSSOR] = lanczos(B,b,500);

figure(3),clf
t = (0:.01:8)';
for i = 1:10
  l = eig(TSSOR(1:i,1:i));
  p = 1;
  for j = i:-1:1
    p = p.*(t-l(j))/(-l(j));
  end
  clf
  plot(real(wwP),imag(wwP),'bx','Markersize',2),hold all
  plot(real(l),imag(l),'r*', 'Markersize',2)
  plot(t,p,'r');
  title(cat(2,'the PCG polynomial p_{',num2str(i),'}'))
  legend('eigenvalues of SA','roots of p_{k}','p_{k}')
  grid on
  box off
  axis([0 2 -1 1]);
  pause(1)
end