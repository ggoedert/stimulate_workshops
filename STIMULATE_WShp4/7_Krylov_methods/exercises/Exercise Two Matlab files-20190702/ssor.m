function x = ssor(b,varargin)

A = varargin{1};
omega = varargin{2};

m = length(b);

x = zeros(m,1);

D = spdiags(diag(A),0,m,m);
E = -tril(A,-1);
F = -triu(A,1);

x = (D-omega*E)\( (omega*F+(1-omega)*D)*x + omega*b);
x = (D-omega*F)\( (omega*E+(1-omega)*D)*x + omega*b);