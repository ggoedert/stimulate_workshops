function x = jacobi(b,varargin)

A = varargin{1};

m = length(b);

D = spdiags(diag(A),0,m,m);

x = D\b;