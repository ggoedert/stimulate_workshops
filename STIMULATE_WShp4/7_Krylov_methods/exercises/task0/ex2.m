N = 16;
A = discreteLaplacian(sqrt(N));
xsol = ones(N,1);

tol = 1E-10;
maxit = 1E3;

%%>> 1) b is a (unif.) random array 
b = rand(N,1);
[x1,flag1,relres1,iter1,resvec1,xvec1] = pcg_loc(A,b,tol,maxit);

% Plot the convergence (relative residue);
subplot(2,2,1)
semilogy([0:iter1],resvec1);
xlabel('# iter (n)')
ylabel('ln(r_n)')
title('b = rand(N,1)')


%%>> 2) b is a (unif.) random array 
[b,D] = eigs(A,1,'sm');
[x2,flag2,relres2,iter2,resvec2,xvec2] = pcg_loc(A,b,tol,maxit);

% Plot the convergence (relative residue);
subplot(2,2,2)
semilogy([0:iter2],resvec2);
xlabel('# iter (n)')
ylabel('ln(r_n)')
title('b = v_1')



%%>> 3) b = e_1 
b= eye(N,1);
[x3,flag3,relres3,iter3,resvec3,xvec3] = pcg_loc(A,b,tol,maxit);
% Plot the convergence (relative residue);
subplot(2,2,3)
semilogy([0:iter3],resvec3);
xlabel('# iter (n)')
ylabel('ln(r_n)')
title('b = e_1')


%%>> 4) b is a (unif.) random array 
[V,D] = eigs(A,2,'sm');
b = V(:,end) + V(:,end-1);
[x4,flag4,relres4,iter4,resvec4,xvec4] = pcg_loc(A,b,tol,maxit);
% Plot the convergence (relative residue);
subplot(2,2,4)
semilogy([0:iter4],resvec4);
xlabel('# iter (n)')
ylabel('ln(r_n)')
title('b = v_1 + v_2')


