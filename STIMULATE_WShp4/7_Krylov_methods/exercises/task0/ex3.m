
N = 16;
A = discreteLaplacian(sqrt(N));
xsol = ones(N,1);

tol = 1E-10;
maxit = 1E3;

%%>> 1) b is a (unif.) random array 
b = rand(N,1);


%%>> 2) b is a (unif.) random array 
[b,D] = eigs(A,1,'sm');

%%>> 3) b = e_1 
b= eye(N,1);

%%>> 4) b is a (unif.) random array 
[V,D] = eigs(A,2,'sm');
b = V(:,end) + V(:,end-1);
