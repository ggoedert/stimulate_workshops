% Eigenvalue localization of CG
% aka the CG polynomial

N = 16;
A = discreteLaplacian(N);
m = size(A,1);
case_b = 2;

if case_b == 1
    b = randn(m,1);
elseif case_b == 2
    [b,D] = eigs(A,1,'sm');
elseif case_b == 3
    b= eye(N^2,1);
elseif case_b == 4
    [V,D] = eigs(A,2,'sm');
    b = V(:,end) + V(:,end-1);
else
    fprintf('chose case 1 by default: b = rand(N,1) \n');
    b = randn(m,1);
end

beta = norm(b);
b = b/beta;

[ T, V] = lanczos(A,b,500);

ww = eig(A);

figure(2),clf
t = (0:.01:8)';
for i = 1:20
   
  l = eig(T(1:i,1:i));
  p = 1;
  for j = i:-1:1
    p = p.*(t-l(j))/(-l(j));
  end
  clf
  plot(real(ww),imag(ww),'bx','Markersize',2), hold all
  plot(real(l),imag(l),'r*', 'Markersize',2)
  plot(t,p,'r');
  title(cat(2,'the CG polynomial p_{',num2str(i),'}'))
  legend('eigenvalues of A','roots of p_{k}','p_{k}')
  grid on
  box off

  axis([0 8 -1 1]);
  pause(1)
end