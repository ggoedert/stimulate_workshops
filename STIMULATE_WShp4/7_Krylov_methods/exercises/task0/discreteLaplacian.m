function A = discreteLaplacian(N)
% Computes the 2D Poisson matrix with dirichlet boundary conditions. 
% Input: dimension N
% Output: A, a N^2xN^2 sparse matrix 

e = ones(N,1);
E = spdiags(e,0,N,N);
F = spdiags([-1*e 2*e -1*e],[-1 0 1],N,N);

A = kron(F,E)+kron(E,F);