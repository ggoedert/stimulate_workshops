function R = testrands(n)
% TESTRANDS
%    R = TESTRANDS(N) produces a row vector of N random numbers with the
%    property that for each number in R its negative also appears in R.
%    In exact arithmetic the sum over all entries of R is zero.
%    The numbers in R are not ordered and are (roughly) taken from the
%    interval [-2^ceil(sqrt(n/2)), 2^ceil(sqrt(n/2))].
%    If N is not an integer it is internally replaces by CEIL(N).

% Author: David Fritzsche (2008)
% Last Change: 12 Dec 2008

    if n <= 0
        error('n must be at least 1.');
    end
    n = ceil(n);
    m = (n - rem(n,2)) / 2;
    k = ceil(sqrt(m));
    X = rand(k);
    X = X .* repmat(2.^(0:k-1),k,1);
    R = reshape(X, 1, numel(X));
    R = R(1:m);
    R = [R -R];
    if length(R) < n
        R = [R zeros(1,n-length(R))];
    end
    R = R(randperm(length(R)));
end
