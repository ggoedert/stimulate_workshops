
n = 1e5;
x = testrands(n);

% WAY 1: naive
sum1 = sum(x);
fprintf('WAY 1:  sum(x) \n s = %g \n',sum1)


% WAY 2: summming in crescent order of x(i)
[~, sort2] = sort(x);
x2 = x(sort2);
sum2 = sum(x2);
fprintf('WAY 2:  summming in crescent order of x(i) \n s = %g \n',sum2)

% WAY 3: summming in crescent order of |x(i)|
[~, sort3] = sort(abs(x));
x3 = x(sort3);
sum3 = sum(x3);
fprintf('WAY 3:  summming in crescent order of |x(i)| \n s = %g \n',sum3)
