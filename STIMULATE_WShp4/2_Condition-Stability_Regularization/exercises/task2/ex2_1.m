
er = zeros(4,1);

for n = 4:2:10
    A = hilb(n);
    sol = ones(n,1);
    b = A*sol;
    x = A\b;

    er(n) = max(abs(x - sol));

end

figure;
semilogy(er,'*')
xlabel('n')
ylabel('Error(n)')