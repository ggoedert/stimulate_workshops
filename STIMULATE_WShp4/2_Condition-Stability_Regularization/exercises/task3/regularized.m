% This script 'solves' a descretized integral equation
% and illustrates the effects of ill-posedness.
alpha_rel = 0.01;
n = input('Enter the number of nodes: ');
s = linspace(0,1,n)';
s = s(2:n-1);
b = (s.^4-2*s.^3+s)/12;
tmp = s*(1-s)';
A = (1/(n-1))*(triu(tmp,0)+triu(tmp,1)');
x = s.*(1-s);
%% first, solve the non-perturbed system
x0 = A\b;
%% now, add some noise
delta1 = 0.01;
alpha1 = alpha_rel * delta1;
delta = delta1*(ones(size(b))-2*rand(size(b)));
B1 = (A'*A + alpha1*eye(size(A)));
b1 = b.*(ones(size(b))+delta);
x1 = B1\b1;

%% now, even more noise
delta2 = 0.05;
alpha2 = alpha_rel * delta2;
delta = delta2*(ones(size(b))-2*rand(size(b)));
B2 = (A'*A + alpha2*eye(size(A)));
b2 = b.*(ones(size(b))+delta);
x2 = B2\b2;
%% plot solution vectors
figure;
x0 = [0; x0; 0];
x1 = [0; x1; 0];
x2 = [0; x2; 0];
s = [0; s; 1];
plot(s,x0,'r-',s,x1,'b-',s,x2,'m-');
legend('unperturbed', 'relative noise: 0.01', ...
       'relative noise: 0.05');
title(['Results for ', num2str(n), ...
       ' nodes, with regularization for \alpha_\delta = ', num2str(alpha_rel), '\delta']);