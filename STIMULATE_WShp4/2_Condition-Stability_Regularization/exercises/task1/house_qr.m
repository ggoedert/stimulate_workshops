function [Q,R] = house_qr(A)

    [m,n] = size(A);
       
    for i = 1:n
    
        x = A(i:m,i);
        v = sign(x(1))*norm(x)*eye(size(x)) + x;
        v = v/norm(v);
        A(i:m,i:n) = A(i:m,i:n) - 2*v*(v'*A(i:m,i:n));
    end

    % TO DO - not enought time
    
end