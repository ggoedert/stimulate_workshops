


n = 80;
[U,X] = qr(randn(n));
[V,X] = qr(randn(n));
S = diag(2.^(-1:-1:-n));

A = U*S*V;

[Qc, Rc] = clgs(A);
[Qm,Rm] = mgs(A);
[Qmt,Rmt] = qr(A);

% Tests
Ac = Qc*Rc;
Am = Qm*Rm;
Amt = Qmt*Rmt;

fprintf('E_clgs = %g \n',max(max(abs(Ac - A))))
fprintf('E_mgs = %g \n',max(max(abs(Am - A))))
fprintf('E_matlab = %g \n',max(max(abs(Amt - A))))

figure; hold on;
plot([1:80],log(diag(abs(Rmt))),'.b')
plot([1:80],log(diag(abs(Rc))),'or')
plot([1:80],log(diag(abs(Rm))),'Xy')
xlabel('n')
ylabel('ln(\sigma_n)')
legend('qr','clgs','mgs')