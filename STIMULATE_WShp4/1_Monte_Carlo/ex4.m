% Author: Guilherme Tegoni Goedert (ESR 07)
% STIMULATE Workshop 4 - Wuppertal
% Lecture 1: Monte Carlo Methods

% Solution to exercise 4
% Repeat the previous exercise using importance sampling by a gaussian
% distribution

mu = 0;
sigma = 1/2;

g = @(x) exp(-2.*norm(x)^2).*(norm(x)^2 + 1)*all((x(:)<1)&(x(:)>-1));

N = 1e5;   % number of random vectors used    
for D = [1,2,10,20]
    
    eta = sigma*randn(N,D) + mu;
    p = @(x) (exp(-norm(x - mu)^2/(2*sigma^2)))./(sqrt(2*pi*sigma^2)^D);
    
    fs = zeros(N,1);
    for i = 1:N
        fs(i) = g(eta(i,:))/p(eta(i,:));
    end
    I = mean(fs);
    varf = mean(fs.^2) - mean(fs).^2;
    error = sqrt(varf/N);
    fprintf('For D = %d,  I([-1,1]^D) = %g +- %g \n',D,I,error)
end