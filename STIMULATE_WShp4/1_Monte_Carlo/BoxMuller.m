% Author: Guilherme Tegoni Goedert (ESR 07)
% STIMULATE Workshop 4 - Wuppertal
% Lecture 1: Monte Carlo Methods

% Solution to exercise 5
% Implementation of the Box Muller algorithm

function [gauss1,gauss2] = BoxMuller(mu,sigma)
s = 2;
while or((s == 0),(s>=1)) 
    u1 = 2*rand - 1;
    u2 = 2*rand - 1;    
    s = u1.^2 + u2.^2;
end

gauss1 = sigma*(u1 * sqrt(-2 * log(s)/s)) + mu;
gauss2 = sigma*(u2 * sqrt(-2 * log(s)/s)) + mu;

end