% Author: Guilherme Tegoni Goedert (ESR 07)
% STIMULATE Workshop 4 - Wuppertal
% Lecture 1: Monte Carlo Methods

% Solution to exercise 1
% Plot the integrands for D = 1 and D = 2

% D = 1
N = 100;
x = linspace(-1 , 1, N);
integrand1 = @(x) exp(-2*abs(x).^2).*(abs(x).^2 + 1);
intg1 = integrand1(x);
figure;
plot(x,intg1)
xlabel('x')
ylabel('integrand')
title('Integrand for D = 1')

% D = 2
N = 100;
x = linspace(-1 , 1, N);
y = linspace(-1 , 1, N);
[X,Y] = meshgrid(x,y);
integrand2 = @(x,y) exp(-2*(x.^2 + y.^2)).*((x.^2 + y.^2) + 1);
intg2 = integrand2(X,Y);
figure;
surf(X,Y,intg2)
xlabel('x')
ylabel('y')
zlabel('integrand')
title('Integrand for D = 2')