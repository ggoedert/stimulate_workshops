% Author: Guilherme Tegoni Goedert (ESR 07)
% STIMULATE Workshop 4 - Wuppertal
% Lecture 1: Monte Carlo Methods

% Solution to exercise 5
% Implement Box_Muller algorithm (done in function-file BoxMuller.m)

N = 1000; 
sample = zeros(2*N,1);

sigma = 0.5;
mu = 0;

for i = 1:N
    
    [sample(2*i-1), sample(2*i)] = BoxMuller(mu,sigma);

end

h = histogram(sample);
fprintf('Mean = %g \n', mean(sample))
fprintf('Std = %g \n', std(sample))
