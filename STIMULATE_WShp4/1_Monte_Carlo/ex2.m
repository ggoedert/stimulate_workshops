% Author: Guilherme Tegoni Goedert (ESR 07)
% STIMULATE Workshop 4 - Wuppertal
% Lecture 1: Monte Carlo Methods

% Solution to exercise 2
% Compute the resulting integral using quad and quad2

% D = 1
integrand1 = @(x) exp(-2*abs(x).^2).*(abs(x).^2 + 1);
I = quad(integrand1,-1,1);
fprintf('For D = 1: \nI(-1,1) = %f \n',I);

%D = 2
integrand2 = @(x,y) exp(-2*(x.^2 + y.^2)).*((x.^2 + y.^2) + 1);
I = quad2d(integrand2,-1,1,-1,1);
fprintf('For D = 2: \nI([-1,1]x[-1,1]) = %f \n',I);
