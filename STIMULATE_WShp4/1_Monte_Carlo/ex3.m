% Author: Guilherme Tegoni Goedert (ESR 07)
% STIMULATE Workshop 4 - Wuppertal
% Lecture 1: Monte Carlo Methods

% Solution to exercise 3
% Use MC Integration for D \in {1,2,10,20}
%     using uniformly distributed random numbers

f = @(x) (exp(-2.*norm(x)^2).*(norm(x)^2 + 1));

N = 1e5;   % number of random vectors used    
for D = [1,2,10,20]
    
    eta = -1 + 2.*rand(N,D);
    p = 1./(2^D);
    
    fs = zeros(N,1);
    for i = 1:N
        fs(i) = f(eta(i,:));
    end
    I = mean(fs./p);
    varf = mean(fs.^2) - mean(fs).^2;
    error = sqrt(varf/N);
    fprintf('For D = %d,  I([-1,1]^D) = %g +- %g \n',D,I,error)
end