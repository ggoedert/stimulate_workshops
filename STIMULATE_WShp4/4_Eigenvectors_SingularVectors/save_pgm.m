function savepgm(pgmfilename, graymap)
  % function savepgm(pgmfilename, graymap)
  % save graymap to pgmfilename, a file in pgm-format
  
  % File:    savepgm.m
  % Author:  Holger Arndt
  % Version: 0.6
  % Date:    24.10.2001

  pgmfile = fopen(pgmfilename, 'w');
  fprintf(pgmfile, 'P5\n');
  width = size(graymap, 2);
  height = size(graymap, 1);
  fprintf(pgmfile, '%i %i\n', width, height);
  fprintf(pgmfile, '%i\n', 255);
  
  imsize = height * width;

  fwrite(pgmfile, reshape(graymap', imsize, 1), 'uchar');

  fclose(pgmfile);
    
