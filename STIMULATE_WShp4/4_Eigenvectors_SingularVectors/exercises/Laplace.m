function A = Laplace( gridSize, dim )

  % returns a sparse matrix corresponding to the finite-difference
  % discretization of the Laplace/Poisson equation in dim-D with gridSize
  % interior points in each direction.
  % Thus, Laplace( 64, 3 ) returns a matrix of size n = 64x64x64

I = speye( gridSize ) ;
e = ones( gridSize, 1 ) ;
B = spdiags( [ -e, 2 * e, -e ], -1 : 1, gridSize, gridSize ) ;

A = B ;
n = gridSize ;

for k = 2 : dim
  A = kron( A, I ) + kron( speye( n ), B ) ;
  n = n * gridSize ;
end

n