

A = gallery('grcar',50);

figure;
[V,D] = eig(A);
subplot(1,2,1);
plot(V,'*');
eigcond = condeig(A);
subplot(1,2,2)
semilogy(sort(eigcond, 'descend'))

% N = [50, 100, 200, 400, 800];
% eigvalues = cell(length(N),1);
% eigcond = zeros(length(N),1);

% figure; hold on;
% for i = 1 : length(N)
% 
%     A = gallery('grcar',N(i));
% 
%     eigvalues{i} = eigs(A);
%     eigcond(i) = condeig(A);
%     semilogy(sort(eigcond(i), 'descend'))
% end
% 
% legends('N = 50','N = 100','N = 200','N = 400','N = 800')
