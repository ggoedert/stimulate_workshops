
N = [16, 32, 64];
times = zeros(size(N));
times_sym = zeros(size(N));

for i = 1:length(N)
    
   A = Laplace(N(i),3);
   tic;
   lambda = eigs(A,10,1);
   times(i) = toc;
   fprintf('2D Laplace %d x %d, 10 eigenvalues closest to 1, exec_time = %g \n',N(i),N(i),times(i));
 
end

log_times = log(times);
figure;
semilogy(N,times,'xb');
xlabel('N');
ylabel('ln(execution time)')
title('execution time of eigs function for A = Laplace(N,3), 10 eigenvalues closest to 1')



%fprintf('We observe that execution time ~ N^3 \n')
