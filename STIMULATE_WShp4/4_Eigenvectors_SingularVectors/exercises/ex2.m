
N = [16, 32, 64, 128, 256];%, 512];
times = zeros(size(N));
times_sym = zeros(size(N));

for i = 1:length(N)
    
   A = Laplace(N(i),2);
   tic;
   lambda = eigs(A);
   times(i) = toc;
   fprintf('2D Laplace %d x %d, exec_time = %g \n',N(i),N(i),times(i));
 
end

figure;
plot(N,times,'xb');
xlabel('N');
ylabel('execution time')
title('execution time of eigs function for A = Laplace(N,2)')

fprintf('We observe that execution time is NOT ~ N^3 \n')
