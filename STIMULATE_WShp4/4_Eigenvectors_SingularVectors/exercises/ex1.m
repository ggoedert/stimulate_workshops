
N = [500 , 1000, 2000, 4000]%, 8000];
times = zeros(size(N));
times_sym = zeros(size(N));

for i = 1:length(N)
    
   A = randn(N(i));
   tic;
   [V,D] = eig(A);
   times(i) = toc;
   fprintf('random %d x %d, exec_time = %g \n',N(i),N(i),times(i));
   
   B = A + A';
   tic;
   [V,D] = eig(B);
   times_sym(i) = toc;
   fprintf('symmetric random %d x %d, exec_time = %g \n',N(i),N(i),times(i));
   
end

figure;
plot(N,times,'xb',N,times_sym,'*r');
xlabel('N');
ylabel('execution time')
title('execution time of eig function for A = rand(N)')
legend('random NxN matrix','symmetric random NxN matrix')

fprintf('We observe that execution time ~ N^3 \n')

