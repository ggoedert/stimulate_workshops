function graymap = loadpgm(pgmfilename)
  %## function graymap = loadpgm(pgmfilename)
  %## loads pgmfile, a file in pgm-format
  
  %## File:    loadpgm.m
  %## Author:  Holger Arndt
  %## Version: 0.6.1
  %## Date:    02.11.2001
  
  pgmfile = fopen(pgmfilename, 'r');
  magicnumber = fgets(pgmfile, 2);
  if magicnumber(1) ~= 'P' || magicnumber(2) ~= '5'
    error('file %s is not in pgm-format', ppmfilename);
  end
  width = [];
  while size(width, 1) == 0
    width = fscanf(pgmfile, '%d', 1);
    if size(width, 1) == 0 % then there is a comment line in the file
      fgetl(pgmfile);
    end
  end
  height = [];
  while size(height, 1) == 0
    height = fscanf(pgmfile, '%d', 1);
    if size(height, 1) == 0
      fgetl(pgmfile);
    end
  end
  maxval = [];
  while size(maxval, 1) == 0
    maxval = fscanf(pgmfile, '%d', 1);
    if size(maxval, 1) == 0
      fgetl(pgmfile);
    end 
  end 
  if maxval ~= 255
    error('unusual file format maxval ~= 255');
  end 

  imsize = height * width;

  fscanf(pgmfile, '%c', 1); % one whitespace
  
  graymap = reshape(fread(pgmfile, imsize, 'uchar'), width, height)';

  fclose(pgmfile);
  
