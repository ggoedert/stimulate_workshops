
Scientific Computing
====================

-   Scientific computing is the art of using digital computers to solve
    problems producing knowledge or products by way of algorithms
    running on silicon (or in the future quantum) logic circuits to
    verify hypothesis or analyze data.
-   Scientific programming is the art of lazy individuals who learn how
    to use a mindless slave machine to work for them performing boring
    tasks.
-   A Scientific program can be reduced to a logical box which is
    programmed to produce one output given an input or instructed to
    generate a random sample input.
-   The Scientific result of the programming effort is thus the program
    output.

Data Format
===========

-   ***Format*** : *The way in which something is arranged or set out*
-   We want the program output data to be arranged for display or
    storage. This process is referred to the formatting
-   Any digital computer platform codifies information as bit sequences,
    for example the __ASCII__ or __UTF-8__ character set codification for text.
    We thus translate number, digits and symbols in bit patterns.
-   These bit patterns are then registered in a stream on a magnetic
    storage and an later interpreted by a rendering engine (screen,
    paper) to produce symbols which we know how to read and interpret.
    Meaningful constructs can be in this way codified in digital form in
    a magnetic memory.

FIXED SIZE
==========

-   We codify a symbol into a fixed storage space with fixed size. The
    basic storage unit size is a byte (8-bit space) and information is
    stored into magnetic memory as multiple of the basic storage unit
    size in blocks of data.
-   Digital computers approximate real numbers with FLOATING POINTS
    numbers, and current implementation of mathematical operations are
    based on algorithms manipulating floating point numbers and integer
    numbers coded in a fixed storage space units (32-64-128 bytes) as
    bit patterns.

Floating Points
===============

-   Using floating point memory representation we have rounding errors.
-   The most common floating point representation is the IEEE 754 in
    which real numbers are base 2 based representations.
-   All numerical operations on floating point numbers follow well
    defined and standard set of rules which are not the real number
    mathematical arithmetic.



```c++
#include <iostream>

std::cout << "C** is at " << std::to_string(__cplusplus) << std::endl;
std::cout << "C++ has IEEE754/559 float standard: " << 
    std::numeric_limits<float>::is_iec559 << std::endl;
std::cout << "The float epsilon is: " << 
    std::numeric_limits<float>::epsilon( ) << std::endl;
std::cout << "The float maximum is: " << 
    std::numeric_limits<float>::max( ) << std::endl;
std::cout << "The float minimum is: " << 
    std::numeric_limits<float>::min( ) << std::endl;
std::cout << "The maximum number of digits is: " << 
    std::numeric_limits<float>::max_digits10 << std::endl;

```

    C** is at 201703
    C++ has IEEE754/559 float standard: 1
    The float epsilon is: 1.19209e-07
    The float maximum is: 3.40282e+38
    The float minimum is: 1.17549e-38
    The maximum number of digits is: 9


The computer represents floating points with bit sequences, and not all real numbers can be exactly represented in base 2:


```c++
#include <iostream>
#include <cstdint>
#include <string>

void DoubleToBinary(const double f, std::string& str)
{
    union { double f; uint64_t i; } u;
    u.f = f;
    str.clear();
    for (int i = 63; i >= 0; i--)
    {
        if (u.i % 2)  str.push_back('1');
        else str.push_back('0');
        u.i >>= 1;
    }
}

{
  constexpr double f64v = 0.1;
  uint64_t u64v;
  std::string bitstring;

  std::memcpy(&u64v,&f64v,sizeof(u64v));
  DoubleToBinary(f64v,bitstring);

  std::cout << std::fixed << f64v << "f64.to_bits() == 0x" <<
      std::hex << u64v << "u64" << std::endl <<
      "bitstring: " << bitstring << std::endl;
}
```

    0.100000f64.to_bits() == 0x3fb999999999999au64
    bitstring: 0101100110011001100110011001100110011001100110011001110111111100


Textual representation
======================

Switching from memory representation of floating point numbers to
character formatted representation introduces rounding errors. Depending
on the application, this can generate in case of numerical experiments
non reproducible results.

Non portable: not all computer platforms use same standard for character
representation.

But ***Science*** has come out of this fact alone!

-   \[...\]Two states differing by imperceptible amounts may eventually
    evolve into two considerably different states ... If, then, there is
    any error whatever in observing the present state—and in any real
    system such errors seem inevitable—an acceptable prediction of an
    instantaneous state in the distant future may well be
    impossible....In view of the inevitable inaccuracy and
    incompleteness of weather observations, precise very-long-range
    forecasting would seem to be nonexistent.\[…\]




```c++
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

{
  float savestep = 55432.01;
  float rstep = 0.0;
    
  // Save a floating point value in a text file
  // file content is 
  std::fstream asave("save.txt",asave.out);
  asave << std::right;
  asave << std::setw(12);
  asave.precision(5);
  asave << savestep << std::endl;
  asave.close( );

  // restart
 
  std::fstream bsave("save.txt",bsave.in);
  bsave >> rstep;
  std::cout << rstep << " " << rstep-savestep;
}
```

    55432.000000 -0.011719

Binary memory dump
==================

-   The only reliable storage format of floating point numbers for the
    experiment to be reproducible using algorithms with non-linear
    differential equations is the one using the memory representation of
    the numbers as in the computer memory.
-   But is it a PORTABLE method? Not all processors and FPU keep in
    memory the bit pattern in the same order (the endianess problem)
-   Not all computer languages write binary record same way (FORTRAN
    unformatted anyone?)
-   Easy way for a customer lock-in. Have you ever spent part of your
    time doing reverse-engineering a binary undocumented commercial
    product format anyone?
-   Have you ever lost dimensional information of a binary written
    tensor?

Open Data
=========

-   Philosophical rant: is Science possible at all if you cannot falsify
    a scientific result?
-   If result are to be published as Science, then the data must be
    public. If not the case, the publication is just not scientific.
    Full Stop.
-   For the data to be Open, the data format should have a complete
    specification document and a reference implementation. All data
    should conform to the specification document and all alternative
    implementation should produce same binary format from the same
    binary input.
-   The on-disk format document should have such a fine level of
    description to allow alternative implementations and grant long term
    storage capabilities even if no software implementations will exist
    on future computing platforms and an interface must be written from
    scratch to access the data.

Standard & Conventions
======================

-   The magic words in the field of Scientific data are
-   STANDARD and CONVENTIONS
-   To have a clear standard allows inter-operability of different
    dataset in a common framework
-   Convention permit definition of physical quantities in a clear,
    unambiguous and application friendly way, so to allow any
    application to read from or write to permanent storage in a
    consistent way.
-   You need not only the data to be stored, but also all relevant
    information about the data
-   Information about the data are the metadata.

Metadata
========

-   Metadata are used to validate the data against a data dictionary for
    consistence, and allow for a clean way for meaningful data sharing
    in a community and between different communities. The metadata
    provide a definitive description of what the data in each variable
    represents, the spatio-temporal properties of the data, the
    description of the measurement device configration, the description
    of the numerical model used, reference to the producer, the
    producing platform, the validity of the data, etc.
-   This enables users of data from different sources to decide which
    quantities are comparable, and facilitates building applications
    with powerful extraction, regridding, and display capabilities.
-   Metadata should be easily parseable by a human, and should in
    principle reduce errors in using the dataset by another scientist
    who has not directly produced the data.
-   The presence of a ***Convention*** allows the data to be
    self-describing in a complete way through consensus agreement on the
    standard defined by all members of an extended scientific community.
    This is usually accomplished by defining what is the minimum subset
    of metadata that is required to successfully self describe a
    dataset, in the sense that each variable has an associated
    description of what it represents, including physical units if
    appropriate, and time and space location if required.
-   Metadata are necessary for falsification of the research.

XML XSD and Conventions
=======================

-   The possibility to syntactically separate the data content from the
    markup allowing structural definitions is key to the possibility of
    having generic syntax checker and generic software library to access
    any document.
-   The XML standardization has successfully posed fot textual dataset a
    convenient way to define a metalanguage which can be successfully
    parsed both by the human mind and the computer programs. Using the
    eXtensible Markup Language any textual data in most human language
    character set using the Unicode character format can be represented
    as data structures.
-   The possibility to have a clean Schema Document and a Document Type
    Definition file allows for consistent way to validate a dataset
    using fixed rules for checking syntax and structure of an XML data
    file.


```c++
#include <iostream>
#include <rapidxml/rapidxml.hpp>
#include <rapidxml/rapidxml_print.hpp>
#include <rapidxml/rapidxml_utils.hpp>
#include <rapidxml/rapidxml_iterators.hpp>

using namespace rapidxml;

{
    xml_document<> doc;
    
    xml_node<>* decl = doc.allocate_node(node_declaration);
    decl->append_attribute(doc.allocate_attribute("version", "1.0"));
    decl->append_attribute(doc.allocate_attribute("encoding", "utf-8"));
    doc.append_node(decl);

    xml_node<>* root = doc.allocate_node(node_element, "Measurement");
    root->append_attribute(doc.allocate_attribute("version", "1.0"));
    root->append_attribute(doc.allocate_attribute("type", "Temperature"));
    root->append_attribute(doc.allocate_attribute("units", "Celsius"));
    doc.append_node(root);
    {
        xml_node<>* m1 = doc.allocate_node(node_element, "Value");
        m1->append_attribute(doc.allocate_attribute("date", "2001-01-01 00:00:00"));
        m1->append_attribute(doc.allocate_attribute("value", "25.05"));
        root->append_node(m1);
    }
    {
        xml_node<>* m1 = doc.allocate_node(node_element, "Value");
        m1->append_attribute(doc.allocate_attribute("date", "2001-01-02 00:00:00"));
        m1->append_attribute(doc.allocate_attribute("value", "24.05"));
        root->append_node(m1);
    }
    std::cout << doc;
}
```

    <?xml version="1.0" encoding="utf-8"?>
    <Measurement version="1.0" type="Temperature" units="Celsius">
    	<Value date="2001-01-01 00:00:00" value="25.05"/>
    	<Value date="2001-01-02 00:00:00" value="24.05"/>
    </Measurement>
    


Dimensionality and Indexing
===========================

-   A factor to consider in storing data on a digital platform is how to
    simply encode in the format what are the dimensions of a particular
    data object. The dimensionality of a data object is not a metadata
    but it is a mandatory information which can prevent a user from
    accessing a dataset.
-   With dimensions come access patterns and indexing or hyper-slabbing
    a dataset

Does a solution exist?
======================

-   Different communities have in-place standards and conventions.
-   Different research fields share similar requirements in term of data
    storage
-   Implementing a generic All Encompassing Hierarchical Object Oriented
    format) was identified in the 1987 as development goal by the
    Graphics Foundations Task Force (GFTF) at the National Center for
    Supercomputing Applications (NCSA) in the United States.
-   Multiple NSF grants allowed for a development effort towards the
    Hierarchical File Format, which has evolved later on through the
    non-profit company HDF Group to the current implementation of the
    much broader target Scientific Data Format known as HDF5.
    
HDF5 file format
================

HDF Group Mission

-   To ensure long term accessibility of HDF data through sustainable
    development and support of HDF technologies
-   Non-profit spin-off in 2006 from NCSA
-   Open Source Business model

HDF5 Sell Sheet
===============

-   Versatile data model that can represent very complex, heterogeneous
    data objects and a wide variety of metadata through an unlimited
    variety of data-types.
-   Ready for high speed raw data acquisition
-   Portable and extensible with no limits on file size, allowing
    application to evolve in their use of HDF5
-   Self describing, requiring no outside information for applications
    to interpret the structure and content of a file
-   Robust software ecosystem of open source tools and applications for
    managing, manipulating, viewing and analyzing data
-   Architecturally independent software library that runs on a wide
    range of computational platforms from laptops to massively parallel
    systems using a wide range of programming languages as C, Fortran,
    C++, Java, Python, R, Ruby, Julia
-   Advanced performance features that allow for access time and storage
    space optimizations through customizable product packaging,
    compression and encryption
-   Long term data archiving solution
-   Consulting, training and support provided by non-profit group
-   Large worldwide user base in wide range of disciplines

HDF5 File Object
================

-   An HDF5 file is an object container that holds a variety of groups
    of heterogeneous data objects (or datasets). The datasets can be
    most anything: images, tables, graphs, or even documents, such as
    PDF.
    
Hyerarchical
============

The HDF file has two primary types of objects:

-   Group
-   Dataset

Groups allow for a POSIX file like structure

-   The dataset is like a file in a directory structure build out of
    groups.

A dataset in a HDF5 file with multiple groups and dataset is identified
in the file by its full qualified path.

HDF5 Group
==========

-   An HDF5 group is very similar to a UNIX directory.
-   All HDF5 files contain at least the root group
-   Any group except the root group is contained in a group
-   A group can contain zero or more named links to other objects
-   A group can have zero or more attributes

Datatype
========

The datatype describe the the individual data elements in a dataset or
attribute. It provides complete information for data conversion to or
from that datatype. They can be:

*Predefined types* defined by the library

-   **Standard datatypes** are the same on all platforms and are what
    you see in an HDF5 file, i.e. H5T\_IEEEF32BE
-   **Native datatypes** are used to simplify memory operations
    (reading, writing) andare NOT the same on different platforms, i.e.
    H5T\_NATIVE\_INT

*Derived Datatypes* are created or derived from the predefined
datatypes. An example of a commonly used derived datatype is a string of
more than one character. Compound datatypes are also derived types.

Dataspace
=========

-   A dataspace is a description of the layout of a dataset’s data
    elements. It can consist of no elements (NULL), a single element
    (scalar), or a simple array.

A dataspace can have dimensions that are fixed (unchanging) or
unlimited, which means they can grow in size (i.e. they are extendible).

Dataspace usage
===============

-   It contains the spatial information (logical layout) of a dataset
    stored in a file. This includes the rank and dimensions of a
    dataset, which are a permanent part of the dataset definition.
-   It describes an application’s data buffers and data elements
    participating in I/O. In other words, it can be used to select a
    portion or subset of a dataset

Properties
==========

-   A property is a characteristic or feature of an HDF5 object. For
    example, the data storage layout property of a dataset is contiguous
    by default. For better performance, the layout can be modified to be
    chunked or chunked and compressed.

HDF5 Attributes
===============

Attributes can optionally be associated with any HDF5 object.

-   They have two parts: a name and a value. Attributes are accessed by
    opening the object that they are attached so are not independent
    objects. Typically an attribute is small in size and contains user
    metadata about the object that it is attached to.

Attributes look similar to HDF5 datasets in that they have a datatype
and dataspace. However, they do not support partial I/O operations, and
they cannot be compressed or extended.

HDF5 Dataset
============

-   An HDF5 dataset is an object composed of a collection of data
    elements, or raw data, and metadata that stores a description of the
    data elements, data layout, and all other information necessary to
    write, read, and interpret the stored data. From the viewpoint of
    the application the raw data is stored as a one-dimensional or
    multi-dimensional array of elements (the raw data), those elements
    can be any of several numerical or character types, small arrays, or
    even compound types similar to C structs. The dataset object may
    have attribute objects.

HDF5 C API
==========

All C routines in the HDF5 library begin with a prefix of the form H5X,
where X is one or two uppercase letters indicating the type of object on
which the function operates.

-   H5F : File interface
-   H5S : Dataspace (memory management) interface
-   H5P : Property interface
-   H5D : Dataset interface
-   H5E : Error interface
-   ...

Wrappers for Fortran, C++, etc.

Open and close an HDF5 file
===========================

The File Access routines allow for creating a new or opening an existing
HDF5 file.

The return handler is of type hid\_t. The HDF5 library error code has
type herr\_t.




```c++
// hid_t H5Fopen( const char *name,
//                unsigned flags,
//                hid_t fapl_id );
```

Flags refer to access mode:

-   H5F\_ACC\_RDWR
-   H5F\_ACC\_RDONLY

Default value for access properties metadata is:

-   H5P\_DEFAULT



```c++
// hid_t H5Fcreate( const char *name,
//                  unsigned flags,
//                  hid_t fcp_id,
//                  hid_t fapl_id );
```

Flags refer to open mode:

-   H5F\_ACC\_TRUNC
-   H5F\_ACC\_EXCL

Default value for access properties and creation properties metadata
are:

-   H5P\_DEFAULT



```c++
// herr_t H5Fclose( hid_t file_id );
```

Property creation
=================



```c++
// hid_t H5Pcreate( hid_t cls_id );
```

Create a property for an object of type identified by its class id.

Properties for dataset creation

-   H5P\_DATASET\_CREATE
-   …



```c++
// herr_t H5Pset_chunk( hid_t plist,
//                      int ndims,
//                      const hsize_t * dim );
```

Sets the size of the chunks used to store a chunked layout dataset.

-   The values of the dim array define the size of the chunks to store
    the dataset's raw data. The unit of measure for dim values is
    dataset elements.

Dataspace management
====================



```c++
// hid_t H5Screate_simple( int rank,
//                         const hsize_t * current_dims,
//                         const hsize_t * maximum_dims );
```

-   Creates a new simple dataspace with requested dimensional properties
    and opens it for access.
-   maximum\_dims for a chunked dataset can have the value
    H5S\_UNLIMITED for any of the dimensions.
-   It must be closed after use.


```c++
// herr_t H5Sclose( hid_t space_id );
```

Dataset creation
================



```c++
// hid_t H5Dcreate2( hid_t loc_id,
//                   const char *name,
//                   hid_t dtype_id,
//                   hid_t space_id,
//                   hid_t lcpl_id,
//                   hid_t dcpl_id,
//                   hid_t dapl_id );
```

Creates a new dataset named name at the location specified by loc\_id,
and associates constant and initial persistent properties with that
dataset, including dtype\_id, the datatype of each data element as
stored in the file; space\_id, the dataspace of the dataset; and other
initial properties as defined in the dataset creation property and
access property lists, dcpl\_id and dapl\_id, respectively. Once
created, the dataset is opened for access.

If a chunked dataset is needed, the dataset creation property list must
have the chunk property in it

Data type id ofr a native floating point value is

-   H5T\_NATIVE\_FLOAT

Default values for properties is

-   H5P\_DEFAULT

Dataset write initialize
========================


```c++
// hid_t H5Dget_space( hid_t dataset_id );
```

-   Returns an identifier for a copy of the dataspace for a dataset. It
    must be closed after use.


```c++
// herr_t H5Sclose( hid_t space_id );

// herr_t H5Sselect_hyperslab( hid_t space_id,
//                             H5S_seloper_t op,
//                             const hsize_t *start,
//                             const hsize_t *stride,
//                             const hsize_t *count,
//                             const hsize_t *block );
```

-   Selects a hyperslab region to add to the current selected region for
    the dataspace specified by space\_id.
-   To start a new selection, the op operation is H5S\_SELECT\_SET.
-   Default value for start, stride, count and block is the NULL
    pointer.

Dataset write
=============


```c++
// herr_t H5Dwrite( hid_t dataset_id,
//                  hid_t mem_type_id,
//                  hid_t mem_space_id,
//                  hid_t file_space_id,
//                  hid_t xfer_plist_id,
//                  const void * buf );
```

-   H5Dwrite writes a (partial) dataset, specified by its identifier
    dataset\_id, from the application memory buffer buf into the file.
    Data transfer properties are defined by the argument
    xfer\_plist\_id. The memory datatype of the (partial) dataset is
    identified by the identifier mem\_type\_id. The part of the dataset
    to write is defined by mem\_space\_id and file\_space\_id.
-   Default value for property is H5P\_DEFAULT
-   dataspace and filespace can be set to H5S\_ALL

Extend dimensions
=================



```c++
// herr_t H5Dset_extent( hid_t dataset_id,
//                       const hsize_t size[] );
```

-   Changes the sizes of a dataset’s on disk dimensions.

Exercise for Day 1
==================

Fill in the following code the missing MPI and HDF5 calls. The code is simulating a model which modifies its status variable (a 2D matrix) splitting the work among N processors.

The complete memory of the problem is allocated on processor 0 and is used to perform the model I/O. The HDF5 is used to write the model status in an output file for ten simulation timesteps.

Note the memory unbalance in the processor 0 and the number of calls only processor 0 is doing related to the I/O. All the other processors do not open the HDF5 file.

Think about the proposed code:

-   How you would improve the model in terms of I/O?
-   What problems are generated by the memory unbalance?
-   How you would improve the MPI layer? Is the communication following a repetitive pattern?


```c++
#pragma cling add_include_path("/opt/anaconda3/include")
#pragma cling add_library_path("/opt/anaconda3/lib")
#pragma cling load("libarmadillo.so")
#pragma cling load("libhdf5.so")
#pragma cling load("libmpi.so")
```


```c++
//
// STIMULATE - Gather data and write
//
// Excercise is create the output HDF5 file and write all the
// "model" timestep to the output file.
// You have as "pseudocode" the HDF5 functions you should call.
//

#include <armadillo>
#include <iostream>
#include <numeric>
#include <vector>
#include <mpi.h>
#include <hdf5.h>

// Constants

#define md_type float
#define md_mpi_type MPI_FLOAT
#define md_h5_type H5T_NATIVE_FLOAT
#define FILE_NAME "model_out.h5"

//
// Main program
//
//int main(int argc, char *argv[])
{
  // Fake arguments
  auto argc = 1;
  auto argv = { "myprog", "" };

  // Global problem size (program arguments)
  const std::size_t tnx = 2000;
  const std::size_t tny = 1000;
  const std::size_t ntimes = 10;
  
  // Exercise target : gather things here
  const int iorank = 0;

  // MPI part
  MPI_Comm global_comm;
  int nproc , rank;

  // Standard MPI initialization
  // (void) MPI_Init(&argc,&argv);
  (void) MPI_Init(&argc, (char ***)(&argv));
  (void) MPI_Comm_dup(MPI_COMM_WORLD, &global_comm);
  (void) MPI_Comm_size(global_comm, &nproc);
  (void) MPI_Comm_rank(global_comm, &rank);

  // Local sizes
  auto nx = tnx / nproc;
  auto ny = tny;

  // Total problem memory
  auto tpm = arma::Mat<md_type>( );
  // Space allocation only on IO-Rank
  if ( rank == iorank ) {
    tpm.set_size(tny,tnx);
  }

  // Better fit the problem: assign leftover rows to the CPUs
  // (max 1 row each)
  auto xstart = rank*nx;
  if ( nx * nproc != tnx ) {
     int xmiss = tnx - nx * nproc;
     if ( nproc < xmiss ) {
       xstart = xstart + rank;
       nx = nx + 1;
     } else {
       xstart = xstart + xmiss;
     }
  }

  // Let all processors have clear view of model topology
  std::vector<int> gsx(nproc);
  std::vector<int> gnx(nproc);
  {
    (void) MPI_Allgather(&xstart,1,MPI_INT,gsx.data( ),1,MPI_INT,global_comm);
    (void) MPI_Allgather(&nx,1,MPI_INT,gnx.data( ),1,MPI_INT,global_comm);
  }

  // Fill total problem memory using armadillo column ordering of Matrix.
  // We want each processor to receive constant values equal to
  // its rank + 1
  if ( rank == iorank ) {
    for ( int ir = 0; ir < nproc; ir ++ ) {
      auto ex = gsx[ir] + gnx[ir] - 1;
      for ( arma::uword i = gsx[ir]; i < ex; i ++ ) {
        for ( arma::uword j = 0; j < ny; j ++ ) {
          tpm(j,i) = (md_type) (ir+1);
        }
      }
    }
  }

  // Processor local memory
  auto lpm = arma::Mat<md_type>(ny,nx);

  exit( 0 );
    
  // Guard value to check communications
  lpm.fill(-100.0);

  // Start out the communication
  // The IO processor sends the data to all the other processors
  
  if ( rank == iorank ) {
    std::cout << "Initial communication." << std::endl;
  }
  std::vector<int> sendcnt(nproc);
  std::vector<int> displs(nproc); 
  for (  int ir = 0; ir < nproc; ir ++ ) {
    // sendcnt[ir] = ???????;
    // displs[ir] = ???????;
  }
  
  // int MPI_Scatterv(const void *sendbuf,
  //                  const int sendcounts[],
  //                  const int displs[],
  //                  MPI_Datatype sendtype,
  //                  void *recvbuf,
  //                  int recvcount,
  //                  MPI_Datatype recvtype,
  //                  int root,
  //                  MPI_Comm comm)

  // Check if communication has been performed : all results should be zero
  if ( int(lpm(0,0)) - rank != 1 ) {
    std::cerr << int(lpm(0,0))
           << " : SEND on proc " << rank << std::endl;
    (void) MPI_Abort(global_comm,-1);
  }

  if ( rank == iorank ) {
    std::cout << "Comm ok." << std::endl;
  }
  // Reset global storage space
  if ( rank == iorank ) {
    tpm.fill(0.0);
  }

  // IO of data using HDF5 library

  hid_t file, dataset, dataspace;
  hid_t filespace, prop;
  herr_t status;

  // Only processor 0 has to open the file with this strategy

  if ( rank == iorank ) {
    // Create the file
    // hid_t H5Fcreate( const char *name,
    //                  unsigned flags,
    //                  hid_t fcpl_id,
    //                  hid_t fapl_id )
    hsize_t dims[3];
    hsize_t maxdims[3];
    hsize_t chunk_dims[3];
    // Set chunking properties
    // hid_t H5Pcreate( hid_t cls_id )
    // herr_t H5Pset_chunk( hid_t plist,
    //                      int ndims,
    //                      const hsize_t * dim )
    // Create dataspace
    // hid_t H5Screate_simple( int rank,
    //                         const hsize_t * current_dims,
    //                         const hsize_t * maximum_dims )
    // Create dataset
    // hid_t H5Dcreate2( hid_t loc_id,
    //                   const char *name,
    //                   hid_t dtype_id,
    //                   hid_t space_id,
    //                   hid_t lcpl_id,
    //                   hid_t dcpl_id,
    //                   hid_t dapl_id )
    // H5Pclose( hid_t prop_id )
  }

  // simulate a model running and compute the new solution

  for ( hsize_t timestep = 0; timestep < ntimes; timestep ++ ) {

    // Fake use of CPU time
    for ( int iloop = 0; iloop < 1000; iloop ++ ) {
      for ( int i = 0; i < nx; i ++ ) {
        for ( int j = 0; j < ny; j ++ ) {
          lpm(j,i) = (md_type) rank+timestep;
        }
      }
    }

    // Collect the new solution on IO processor
    // int MPI_Gatherv( const void *sendbuf,
    //                  int sendcount,
    //                  MPI_Datatype sendtype,
    //                  void *recvbuf,
    //                  const int recvcounts[],
    //                  const int displs[],
    //                  MPI_Datatype recvtype,
    //                  int root,
    //                  MPI_Comm comm)
    
    // Assert communication is ok
    if ( rank == iorank ) {
      for ( int ir = 0; ir < nproc; ir ++ ) {
        if ( int(tpm(0,gsx[ir])) != ir + timestep ) {
          std::cerr << int(tpm(0,gsx[ir])) << " " << ir + 2
                    << " : ERROR from proc " << ir << std::endl;
          (void) MPI_Abort(global_comm,-1);
        }
      }
    }

    // Write new solution as timestep in the HDF5 file
    if ( rank == iorank ) {
      // Get filespade
      // hid_t H5Dget_space( hid_t dataset_id )
      hsize_t offset[3];
      hsize_t count[3];
      // Select destination hyperslab
      // herr_t H5Sselect_hyperslab( hid_t space_id,
      //                             H5S_seloper_t op,
      //                             const hsize_t *start,
      //                             const hsize_t *stride,
      //                             const hsize_t *count,
      //                             const hsize_t *block )
      // Create dataspace
      // hid_t H5Screate_simple( int rank,
      //                         const hsize_t * current_dims,
      //                         const hsize_t * maximum_dims )
      // Write data
      // herr_t H5Dwrite( hid_t dataset_id,
      //                  hid_t mem_type_id,
      //                  hid_t mem_space_id,
      //                  hid_t file_space_id,
      //                  hid_t xfer_plist_id,
      //                  const void * buf )
      std::cout << "Timestep " << timestep << std::endl;
      if ( timestep < ntimes - 1 ) {
        hsize_t newdims[3];
        // Extend dataset
        // herr_t H5Dset_extent( hid_t dataset_id,
        //                       const hsize_t size[] )
      }
      // H5Sclose( hid_t space_id )
      // H5Sclose( hid_t space_id )
    }

  } // time loop

  // The Armadillo library has a dump helper in HDF5 format ;)
  //if ( rank == iorank ) {
  //  tpm.save(arma::hdf5_name("tpm.h5", "tpm"));
  //}

  // Resource cleanup
  // Note that processor 0 is unbalanced and has a lot more resources.
  if ( rank == iorank ) {
    // H5Dclose( hid_t dset_id );
    // H5Fclose( hid_t file_id );
    tpm.reset( );
    std::cout << "Model succesfully completed." << std::endl;
  }
  lpm.reset( );

  (void) MPI_Finalize();
}
```
